require("./sub-systems/logging-setup.js");
var RSVP = require("rsvp");
const StartUpManager = require("./sub-systems/StartUpManager");
var ModuleManager = require("./sub-systems/ModuleManager");
var Additive = require("./modules/Additive");
var RefillAdditive = require("./modules/RefillAdditive");
var Gantry = require("./modules/Gantry");
var Deck = require("./modules/Deck");
var Tower = require("./modules/Tower");
var Reception = require("./modules/Reception");
var Lighting = require("./modules/Lighting");
var Scanner = require("./modules/Scanner");
var MovementController = require("./sub-systems/MovementController");
var DrinkController = require("./sub-systems/DrinkController");
var OrderServer = require("./sub-systems/OrderServer");
var LosantReporter = require("./sub-systems/LosantReporter");
var NoOpPaymentProvider = require("./sub-systems/payment-providers/NoOpPaymentProvider");
var WorldnetPaymentProvider = require("./sub-systems/payment-providers/WorldnetPaymentProvider");
var inventoryPersistence = require("./sub-systems/InventoryPersistence");
var faultManager = require("./sub-systems/FaultManager.js");
var postal = require("postal");
var winston = require("winston");
var jsonfile = require("jsonfile");
const util = require("util");
var postalChannel = postal.channel("kioskControl");

var reporter = new LosantReporter();
reporter.sendUpdate();
reporter.sendPriceUpdate();

console.log("publishing startup");

var deck;
var gantry;
var tower;
var lighting;
var additive;
var refillAdditive;
var reception;
var scanner;

function beginStartup(){
  postalChannel.publish("startup");

  ModuleManager.registerModule("D", function (port) {
    deck = new Deck(port);
  });

  ModuleManager.registerModule("G", function (port) {
    gantry = new Gantry(port);
  });

  ModuleManager.registerModule("T", function (port) {
    tower = new Tower(port);
  });

  ModuleManager.registerModule("L", function (port) {
    lighting = new Lighting(port);
  });

  ModuleManager.registerModule("A", function (port) {
    additive = new Additive(port, inventoryPersistence.getData().additives);
  });

  ModuleManager.registerModule("B", function (port) {
    refillAdditive = new RefillAdditive(port, inventoryPersistence.getData().additives);
  });

  ModuleManager.registerModule("R", function (port) {
    reception = new Reception(port);
  });

  ModuleManager.registerScanner(function (port) {
    scanner = new Scanner(port, false);
  });


  // Done registering modules, begin monitoring
  ModuleManager.beginMonitoring();


var paymentProviderData = jsonfile.readFileSync("payment-provider.json");
var paymentProvider;
if(paymentProviderData.payment == "paid"){
  //Use worldnetPaymentProvider for card reader stuff
  paymentProvider = new WorldnetPaymentProvider();
}else{
  //Set to "free"
  //Use NoOpPaymentProvider for free kiosk behavior, it bypasses the card reader stuff
  paymentProvider = new NoOpPaymentProvider();
}

paymentProvider.connectToCardReader().then(function(){
  winston.log("debug", "cardReaderConnectedCheck hit");
  postalChannel.publish("cardReaderConnected");
  // postalChannel.publish("cardReaderConnected");
  
})

//Module simulator startup
/*
var deck = new Deck("temp");
var gantry = new Gantry("temp");
var tower = new Tower("temp");
var additive = new Additive("temp", inventoryPersistence.getData().additives);
var reception = new Reception("temp");
*/

var startupFaultTimeout = setTimeout(function(){
  //checks for remaining faults if we dont bootup after 20 seconds
  var activeFaults = faultManager.getActiveFaults()
  if(activeFaults.length > 0){
    //fault on bootup
    winston.log("debug", "active faults remain on bootup", activeFaults);
  }
},20000)

StartUpManager.allResolved().then(function(){
  clearTimeout(startupFaultTimeout);
  winston.log("debug", "all good to go");
  var movementController = new MovementController(gantry, tower, deck, reception, lighting);
  var drinkController = new DrinkController(additive, deck, refillAdditive);
  var orderServer = new OrderServer(paymentProvider, movementController, drinkController);


  gantry.tower = tower; // Connect tower to gantry
  tower.gantry = gantry;// Connect gantry to tower

  refillAdditive.enableStillWater().then(function(){
    refillAdditive.setLightColor("B").then(function(){
      refillAdditive.sendRefillOrder("water");
    });
  });

  movementController.finishStartupSequence().then(function(){
    winston.log("debug", "publishing ready");
    postalChannel.publish("ready");
    });
  });
}

postal.subscribe({
  channel: "order-server",
  topic: "orderComplete",
  callback: function(orderCompleteStatus) {
    if(orderCompleteStatus == "success"){
      reporter.sendUpdate();
    }
  }
});

postal.subscribe({
  channel: "UIServer",
  topic: "websocketConnectionComplete",
  callback: function() {
    beginStartup();
  }
});

postal.subscribe({
  channel: "UIServer",
  topic: "maintenanceCommandStart",
  callback: function(command) {
    console.log(command);
    onMaintenanceCommandStart(command)
  }
});

postal.subscribe({
  channel: "UIServer",
  topic: "maintenanceCommandEnd",
  callback: function(command) {
    console.log(command);
  }
});

postal.subscribe({
  channel: "scanner",
  topic: "unlock",
  callback: function() {
    if(lighting != null){
      lighting.unlockDoor();
    }
  }
});

postal.subscribe({
  channel: "UIServer",
  topic: "unlock",
  callback: function() {
    if(lighting != null){
      lighting.unlockDoor();
    }
  }
});

function onMaintenanceCommandStart(command){
  winston.log("debug", command.action);
  switch(command.action){
    case "raiseLeftTower":
      tower.raiseLeftTower();
      break;
    case "raiseRightTower":
      tower.raiseRightTower();
      break;
    case "lowerLeftTower":
      tower.resetLeftTower();
      break;
    case "lowerRightTower":
      tower.resetRightTower();
      break;
    case "purgeAdditive1Refill":
      refillAdditive.setPurge().then(function(){
        refillAdditive.sendPurgeCode("~0000000")
      })
      break;
    case "purgeAdditive2Refill":
      refillAdditive.setPurge().then(function(){
        refillAdditive.sendPurgeCode("0~000000")
      })
      break;
    case "purgeAdditive3Refill":
      refillAdditive.setPurge().then(function(){
        refillAdditive.sendPurgeCode("00~00000")
      })
      break;
    case "purgeAdditive4Refill":
      refillAdditive.setPurge().then(function(){
        refillAdditive.sendPurgeCode("000~0000")
      })
      break;
    case "purgeAdditive5Refill":
      refillAdditive.setPurge().then(function(){
        refillAdditive.sendPurgeCode("0000~000")
      })
      break;
    case "purgeAdditive6Refill":
      refillAdditive.setPurge().then(function(){
        refillAdditive.sendPurgeCode("00000~00")
      })
      break;
    case "flushRefillAmbient":
      refillAdditive.sendRefillTemperature("room").then(function(){
        refillAdditive.sendWaterDefault();
      })
      break;
    case "flushRefillCold":
      refillAdditive.sendRefillTemperature("cold").then(function(){
        refillAdditive.sendWaterDefault();
      })
      break;
    default:
      winston.log("debug", "received command from maintenance that is not currently supported", command);
  }
}


function onMaintenanceCommandEnd(command){

}
