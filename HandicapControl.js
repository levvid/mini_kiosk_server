var postal = require("postal");
let {PythonShell} = require("python-shell");


var postalChannel = postal.channel("handicapControl")
let pyshell = new PythonShell('buttonInput.py', { mode: 'text', pythonOptions: ['-u']});

//let pyshell = new PythonShell('easyPy.py', { mode: 'text', pythonOptions: ['-u']});
pyshell.on('message', function (message) {
  // received a message sent from the Python script (a simple "print" statement)
  postalChannel.publish("buttonPress", message);
});

// end the input stream and allow the process to exit
pyshell.end(function (err,code,signal) {
  if (err) throw err;
  console.log('The exit code was: ' + code);
  console.log('The exit signal was: ' + signal);
  console.log('finished');
});
