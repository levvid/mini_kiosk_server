//Drop-Kiosk-UI-Server

var express = require("express");
var app = express();
var http = require("http").createServer(app);
var path = require("path");
var bodyParser = require("body-parser");
var io = require("socket.io")(http);
var kioskControl = require("./KioskControl.js")
var LosantReporter = require("./sub-systems/LosantReporter");
var handicapControl = require("./HandicapControl.js");
var postal = require("postal");
var jsonfile = require("jsonfile");
var winston = require("winston");

var fs = require('fs') 


var postalChannel = postal.channel("UIServer");

// disable gestures
// global.stage.get_actions().forEach(a => a.enabled = false);


app.use(express.static(path.join(__dirname, '/public')));
console.log("dirname", __dirname);
app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
}));

//Serve the main interface page on the request
app.get("/", function(req, res){
  res.sendFile(path.join(__dirname, '/Drop-Kiosk-UI.html'));
  winston.log("debug", "serving order page");
});

//Serve the error page on the request
app.get("/error", function(req, res){
  res.sendFile(path.join(__dirname, '/ErrorTest.html'));
  winston.log("debug", "serving error page");
});

//Serve the startup page on the request
app.get("/startup", function(req, res){
  res.sendFile(path.join(__dirname, '/Startup.html'));
  winston.log("debug", "serving startup page");
});

//Serve the maintenance page on the request
app.get("/maintenance", function(req, res){
  res.sendFile(path.join(__dirname, '/Maintenance.html'));
  winston.log("debug", "serving maintenance page");
});

app.get("/flavors", function(req, res) {
  //Returns flavors to the main html page
  var flavorArray = getFlavorList();
  console.log(flavorArray);
  res.send(JSON.stringify(flavorArray));
  console.log("sending flavors");
});


app.get("/inventoryForMaintenance", function(req,res){
  var inventoryData = jsonfile.readFileSync("inventory.json");
  res.send(JSON.stringify(inventoryData));
  console.log("sending inventory to maintenance");
});

app.get("/getPrice", function(req, res) {
  //returns drink prices to the mainHTML page
  var priceJson = jsonfile.readFileSync("drinkPrice.json");

  res.send(JSON.stringify(priceJson));
  console.log("sending price")
});

//Connecting to the server
http.listen(3000, function(){
  console.log('listening on *:3000');
});


//Receiving order data from main order screen
app.post("/order", function(req, res) {
  console.log(req.body.flavor);
  console.log(req.body.caffeine);
  console.log(req.body.temperature);
  console.log(req.body.dropBottle);
  console.log(req.body.orderPrice);
  var response = JSON.stringify({dropBottle: req.body.dropBottle, flavor: req.body.flavor, caffeine: req.body.caffeine, temperature: req.body.temperature, orderPrice:req.body.orderPrice});
  res.send(response);
  var price = req.body.orderPrice;
  var orderObject = {dropBottle: req.body.dropBottle, flavor: req.body.flavor, caffeine: req.body.caffeine, temperature: req.body.temperature, orderPrice:req.body.orderPrice};
  postalChannel.publish("new", orderObject);
});

app.post("/updateInventoryFile", function(req, res) {
  console.log(req.body);
  jsonfile.writeFileSync("inventory.json", req.body, {spaces: 2});
  var response = JSON.stringify("Success");
  res.send(response);
});

app.post("/updateDrinkPrice", function(req, res) {
  console.log(req.body);
  jsonfile.writeFileSync("drinkPrice.json", req.body, {spaces: 2});
  var drinkPrice = req.body;
  var paymentProvider;
  if(drinkPrice.dropBottle.justWaterPrice == 0 && drinkPrice.dropBottle.flavorPrice == 0 && drinkPrice.dropBottle.supplementPrice == 0 && drinkPrice.ownBottle.justWaterPrice == 0 && drinkPrice.ownBottle.flavorPrice == 0 && drinkPrice.ownBottle.supplementPrice == 0){
    paymentProvider = "free";
  }else{
    paymentProvider = "paid";
  }
  paymentJSON = {
                  payment: paymentProvider
                }
  jsonfile.writeFileSync("payment-provider.json", paymentJSON, {spaces: 2});
  var response = JSON.stringify("Success");
  res.send(response);
});

app.post("/cancelOrder", function(req, res) {
  winston.log("debug", "cancel current order hit");
  postalChannel.publish("cancelCurrentOrder");
  var response = JSON.stringify("Success");
  var payload =
    {
      action: "cancel-transaction"
    }
  postalChannel.publish("Worldnet-Message", payload);
  res.send(response);
});

app.post("/worldnetMessage", function(req, res) {
  var worldnetMessage = (req.body);
  if(worldnetMessage.worldnetResponse.type == "WEBSOCKET_CONNECTION_ERROR"){
    //todo: track this error and dont allow the kiosk to boot
    winston.log("debug", "websocket connection error");
  }else{
    winston.log("debug", "hit connection success in post in uiserver file");
    sendWorldnetMessageToWebPage(worldnetMessage.worldnetResponse);
    postalChannel.publish("worldnet-message", worldnetMessage.worldnetResponse);
  }
  var response = JSON.stringify("Success");
  res.send(response);
});

//connect to communicate with html pages about status
io.on("connection", function(socket){
  sendDropWSConnect();
  postalChannel.publish("websocketConnectionComplete");
  console.log("a user connected");
  socket.on("StatusMessage", function(msg){
    console.log("Status Message: ", msg);
  });
  socket.on("maintenanceButtonTouchStart", function(msg){
    winston.log("debug", "maintenanceButton touch start hit");
    postalChannel.publish("maintenanceCommandStart", msg);
  });
  socket.on("maintenanceButtonTouchEnd", function(msg){
    postalChannel.publish("maintenanceCommandEnd", msg);
  });
  socket.on("exitMaintenanceMode", function(msg){
    var exec = require("child_process").exec;
    //reboots the whole machine on exit maintenance mode
    exec("sh /usr/local/bin/reboot.sh");
  });
  socket.on("dispenseStart", function(){
    winston.log("debug", "dispensing begins");
    postalChannel.publish("refillDispenseStart");
  });
  socket.on("dispenseEnd", function(){
    winston.log("debug", "dispensing ends");
    postalChannel.publish("refillDispenseEnd");
  });
  socket.on("dispenseCompleted", function(){
    winston.log("debug", "dispense complete button pressed");
    postalChannel.publish("refillDispenseCompleted");
  });

  socket.on("openKioskDoor", function(msg){
    winston.log("debug", "maintenanceButton touch start hit");
    postalChannel.publish("unlock");
  });

});


//Get startup status from the kioskControl File
postal.subscribe({
  channel: "kioskControl",
  topic: "startup",
  callback: function() {
    sendStatusStartup("");
  }
});

//Get the ready status from the kioskControl file
postal.subscribe({
  channel: "kioskControl",
  topic: "ready",
  callback: function() {
    sendStatusReady();
  }
});


postal.subscribe({
  channel: "scanner",
  topic: "enterMaintenance",
  callback: function() {
    enterMaintenanceMode();
  }
});

//Get the ready status from the kioskControl file
postal.subscribe({
  channel: "kioskControl",
  topic: "cardReaderConnected",
  callback: function() {
    sendStatusCardReaderConnected();
  }
});



//Get the payment status from the OrderServer file
postal.subscribe({
  channel: "order-server",
  topic: "scanned-order",
  callback: function(orderData) {
    //success can either be true or false
    if(orderData == "invalid"){
      //show error message
    }else{
      //valid order data display dispensing animation
      sendScannedOrder(orderData);
    }
  }
});

//Subscribe to the order server
postal.subscribe({
  channel: "order-server",
  topic: "orderComplete",
  callback: function(message) {
    console.log("Order message: ", message);
    sendOrderCompleteStatus(message);
  }
});

//Get the module startup status from the ModuleManager file
postal.subscribe({
  channel: "moduleManager",
  topic: "moduleSetup",
  callback: function(module) {
    sendStatusStartup(module);
  }
});

//Get the module startup status from the ModuleManager file
postal.subscribe({
  channel: "handicapControl",
  topic: "buttonPress",
  callback: function(button) {
    console.log("handicap button pressed: ", button);
    io.emit("HandicapControl", button);
  }
});

//Subscribe to the payment status
postal.subscribe({
  channel: "order-server",
  topic: "payment",
  callback: function(message) {
    // msg is either "success" or false
    console.log("payment Message: ", message);
    sendPaymentStatus(message);
  }
});


//Subscribe to the payment status
postal.subscribe({
  channel: "order-server",
  topic: "error",
  callback: function(errorMessage) {
    winston.log("debug", "order server error message: ", errorMessage);
    sendErrorMessage(errorMessage);
  }
});

//Subscribe to the payment status
postal.subscribe({
  channel: "status",
  topic: "error",
  callback: function(errorMessage) {
    winston.log("debug", "status error message testing UI: ", errorMessage);
    sendErrorMessage(errorMessage);
  }
});

//Subscribe to the drinkController refill Dispense Data
postal.subscribe({
  channel: "order-server",
  topic: "refillData",
  callback: function(refillData) {
    winston.log("debug", "refillData dispenseData: ", refillData);
    sendRefillData(refillData);
    }
});

//Subscribe to the drinkController refill Dispense Data
postal.subscribe({
  channel: "drinkController",
  topic: "refillInactivityTimeoutHit",
  callback: function() {
    sendRefillData("refillInactivityTimeoutHit");
    }
});

//Subscribe to the worldnet messages
postal.subscribe({
  channel: "worldnet-provider",
  topic: "worldnet-message",
  callback: function(msg) {
    sendWorldnetMessageToWebPage(msg);
    }
});


function sendErrorMessage(){
  //Send error message to the html page to tell it to request error page from server
  io.emit("Error", "This is the error message");
}
function sendStatusReady(){
  //Send ready message to the html page to tell it to request ready page from server
  io.emit("Ready", "Everything is ready to go");
}

function sendStatusCardReaderConnected(){
  //Send ready message to the html page to tell it to request ready page from server
  winston.log("debug", "emitting cardreaderconnected to webpage");
  io.emit("CardReaderConnected", "Reader is ready to go");
}

function sendStatusStartup(msg){
  //Send startup message to the html page to tell it to request startup page from server
  console.log("sending startu0 msg");
  io.emit("Startup", msg);
}

function sendDropWSConnect(msg){
  //Send startup message to the html page to tell it to request startup page from server
  io.emit("dropWSConnect", msg);
}

function sendPaymentStatus(msg){
  //Send payment status message to the html page
  io.emit("Payment", msg);
}

function sendScannedOrder(msg){
  //Send scanned order to the html page to process order
  io.emit("ScannedOrder", msg);
}

function sendOrderCompleteStatus(msg){
  //Send startup message to the html page to tell it to request startup page from server
  io.emit("OrderComplete", msg);
}

function sendWorldnetMessageToWebPage(msg){
  io.emit("WorldnetMessage", msg);
}

function sendRefillData(msg){
  io.emit("RefillData", msg);
}



function getFlavorList(){
  var inventoryData = jsonfile.readFileSync("inventory.json");
  var additiveLookup = jsonfile.readFileSync("additiveLookup.json");
  var lookupArray = additiveLookup.additives;
  var additiveArray = inventoryData.additives;
  var flavorArray = [];
  additiveArray.forEach(function(additive){
    lookupArray.forEach(function(lookup){
      if(additive.name == lookup.name && lookup.type == "flavor"){
        flavorArray.push(additive);
      }
    });
  });
  return flavorArray;
}

function enterMaintenanceMode() {
  // send enterMaintenanceMode message
  winston.log("debug", "Emmitting enter maintenance mode signal");
  io.emit("EnterMaintenanceMode", "Admin user to enter maintenance");
}
