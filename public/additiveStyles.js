
var additiveStyleArray =
[
  {
    flavor: "water",
    ingredients: "Water",
    dropShadowHue: "#cff6ff",
    thankYouTextColor: "#5fd4e0",
    enjoyTextColor: "#6ce3ff"
  },
  {
    flavor: "cucumber",
    ingredients: "Water, Natural Flavors",
    dropShadowHue: "#dbfa98",
    thankYouTextColor: "#c6e583",
    enjoyTextColor: "#d0f77d"
  },
  {
    flavor: "lemonade",
    ingredients: "Water, Natural Flavors, Citric Acid, Malic Acid, Sodium Citrate, Stevia",
    dropShadowHue: "#fcf997",
    thankYouTextColor: "#f4ec62",
    enjoyTextColor: "#f7f479"
  },
  {
    flavor: "guava",
    ingredients: "Water, Natural Flavors, Malic Acid, Citric Acid, Sodium Citrate, Stevia",
    dropShadowHue: "#ffd9d3",
    thankYouTextColor: "#ea7663",
    enjoyTextColor: "#f99f91"
  }
]
