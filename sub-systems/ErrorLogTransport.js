var winston = require("winston");
var util = require("util");
var postal = require("postal");

// A custom logger transport that only receives error level logs.
// It then publishes onto the "status" postal channel to notify about fatal error.

var ErrorLogTransport = function() {
  this.name = "errorLogTransport";

  // Only get the error level logs
  this.level = "error";
};

util.inherits(ErrorLogTransport, winston.Transport);

/**
  Publish the incoming log `message` and `meta` object to the Postal "status"
  channel.
  @method log
*/
ErrorLogTransport.prototype.log = function(level, msg, meta, callback) {
  postal.publish({
    "channel": "status",
    "topic": "error",
    "data": {
      "message": msg,
      "meta": meta
    }
  });
  callback(null, true);
};

module.exports = ErrorLogTransport;
