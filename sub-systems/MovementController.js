"use strict";

var RSVP = require("rsvp");
var winston = require("winston");
var jsonfile = require("jsonfile");
var inventoryPersistence = require("./InventoryPersistence")
var postal = require("postal");
var postalChannel = postal.channel("movement-controller");

function MovementController (gantry, tower, deck, reception, lighting){
  this.gantry = gantry;
  this.tower = tower;
  this.deck = deck;
  this.reception = reception;
  this.lighting = lighting;

  this.bottleReadyToUncapPromise = null;
  this.nextBottleClosedPromise = null;
}

/**
  @method: finishStartupSequence
  @description: Called on boot to make sure machine is ready to dispens
    a drink.
*/
MovementController.prototype.finishStartupSequence = function(){
  var self = this;
  winston.log('debug', "finishStartupSequence hit");
  return new Promise(function(resolve, reject){
    //Also check if deck has cap in it
    self.gantry.sendCurrentTower().then(function(){
      self.gantry.checkIfHoldingBottle().then(function(response){
        self.checkIfReceptionHasBottle().then(function(code){
          if(code == "C"){
            winston.log("debug", "reception has bottle");
            if(response == "K"){
              //gantry has bottle in it
              resolve(self.uncapOnStartup());
            }else{
              //Gantry does not have bottle
              self.stageBottleForFilling().then(function(){
                self.retrieveAndCloseBottleSuccess().then(function(){
                  resolve();
                });
              });
            }
          }else{
            winston.log("debug", "reception does not have bottle");
            if(response == "N"){
              //Gantry does not have bottle
              self.retrieveAndCloseBottleSuccess().then(function(){
                self.gantryDropBottleInSlider().then(function(){
                  self.stageBottleForFilling().then(function(){
                    self.retrieveAndCloseBottleSuccess().then(function(){
                      resolve();
                    });
                  });
                });
              });
            }else{
              //gantry does have bottle
              self.gantryDropBottleInSlider().then(function(){
                self.stageBottleForFilling().then(function(){
                  self.retrieveAndCloseBottleSuccess().then(function(){
                    resolve();
                  });
                });
              });
            }
          }
        });
      });
    });
  })
};

/**
  @method: retrieveCloseAndUncapNextBottle
  @description: raises tower if necessary, then retreives and flap seals
  the bottle, then uncaps it in preperation for filling
*/
MovementController.prototype.retrieveAndCloseNextBottle = function(){
  var self = this;
  return new Promise(function(resolve, reject){
    self.gantry.retrieveNextBottleSuccess().then(function(){
      //Single Bottle
      winston.log("debug", "retrieve bottle success");
      self.gantry.closeBottle().then(function(code){
        winston.log("debug", "gantry closing bottle");
        if(code == "E"){
          winston.log('error', "error closing bottle");
          postalChannel.publish("closeBottle", "error");
          reject(code);
          //Garbage?
        }else{
          resolve();
        }
      });
    });
  });
};

/**
  Used to drop off a bottle in the slider on reset if there's no
  bottle in the slider

  @method gantryDropBottleInSlider
  @return {Promise} Resolves with K when the bottle is dropped off and moves to Deck.
*/
MovementController.prototype.gantryDropBottleInSlider = function() {
  var gantry = this.gantry;
  var deck = this.deck;
  var self = this;
  return new Promise(function (resolve) {
    deck.moveToDropOff().then(function(){
      gantry.deliverBottle().then(function(){
        self.sanitizeProbe();
        deck.moveToElevator().then(function(response){
          resolve(response);
        });
      });
    });
  });
};

MovementController.prototype.uncapOnStartup = function() {
  var reception = this.reception,
      deck = this.deck,
      self = this;

  var startupUncappedPromise = new RSVP.Promise(function (resolve, reject) {
    let uncapAttemptCounter = 0;
    deck.capCheck().then(function(value){
      winston.log("info", "data from cap check", value);
      if(value === "N"){
        uncapProcedure(resolve, reject); // Kick off first uncapProcedure after bottle is ready
      }else{
        resolve(1);
      }
    });
    function uncapProcedure (resolve, reject) {
      if (uncapAttemptCounter === 5) {
        // Limit the uncap attempts to 5
        winston.log("debug", "MovementController Bottle Capper", {"bottleUncapAttemptCount": uncapAttemptCounter});
        winston.log("error", "MovementController Uncap Attempt Exceeded Test startup");
        postalChannel.publish("uncap", "error");  
        winston.log("debug", "After uncap error");
        reject("MovementController Uncap Attempt Exceeded reject");
        return;
      }
      uncapAttemptCounter++;
      self.uncapBottleAttempt().then(function() {
        return deck.capCheck();
      }).then(function (code) {
        if (code === "C") {
          winston.log("debug", "MovementController Bottle Capper capper", {"bottleUncapAttemptCount": uncapAttemptCounter});
          // Cap is detected in the sensor
          // Cap has been removed from the bottle
          resolve(uncapAttemptCounter); // Cap check successful
        } else {
          uncapProcedure(resolve, reject); // Cap check failed, try uncapping again
        }
      });
    }
  });

  return startupUncappedPromise.then(function(){
    reception.moveToCapperForCapping();
  });
};

/**
  uncap bottle then raise elevator slightly this will prepare it to be filled

  @method stageBottleForFilling
  @return {Promise} Resolves when bottle is staged.
*/
MovementController.prototype.stageBottleForFilling = function() {
  var deck = this.deck,
      reception = this.reception,
      gantry = this.gantry,
      lighting = this.lighting,
      self = this;

  // Try uncapping bottle until it is confirmed as uncapped
  winston.log("info", "staging bottle");
  var uncappedPromise = new RSVP.Promise(function (resolve, reject) {
    let uncapAttemptCounter = 0;
    deck.capCheck().then(function(value){
      winston.log("info", "data from cap check", value);
      if(value === "N"){
        uncapProcedure(resolve, reject); // Kick off first uncapProcedure after bottle is ready
      }else{
        resolve(1);
      }
    });
    function uncapProcedure (resolve, reject) {
      if (uncapAttemptCounter === 5) {
        // Limit the uncap attempts to 5
        winston.log("debug", "MovementController Bottle Capper", {"bottleUncapAttemptCount": uncapAttemptCounter});
        winston.log("error", "MovementController Uncap Attempt Exceeded bottle for filling");
        postalChannel.publish("uncap", "error"); 
        winston.log("debug", "After uncap lower"); 
        reject("MovementController Uncap Attempt Exceeded");
        return;
      }
      uncapAttemptCounter++;
      self.uncapBottleAttempt().then(function() {
        return deck.capCheck();
      }).then(function (code) {
        if (code === "C") {
          winston.log("debug", "MovementController Bottle Capper", {"bottleUncapAttemptCount": uncapAttemptCounter});
          // Cap is detected in the sensor
          // Cap has been removed from the bottle
          resolve(uncapAttemptCounter); // Cap check successful
        } else {
          uncapProcedure(resolve, reject); // Cap check failed, try uncapping again
        }
      });
    }
  }, "MovementController prepareBottleForFilling upcappedPromise"); // RSVP Promise label for debugging

  return uncappedPromise.then(function() {
    //slide the bottle over to the drop off POSITION
    return deck.moveToDropOff().then(function(){
      //probeBottle with gantry
      return gantry.probeBottle().then(function(){
        lighting.turnOffUVLight();
        //move bottle back to elevator with slider
        return deck.moveToElevator().then(function(){
          //use this to raise the capper to a holding stage
          return reception.moveToCapperForCapping();
        });
      });
    });
  }).then(function() {
    return winston.log("info","removed cap probed bottle and raised bottle into staging position");

  });
};


/**
  Used to check to see if bottle under the capper uncapper on startup
  so that we can see if we need to load another bottle

  @method checkIfReceptionHasBottle
  @return {Promise} Resolves with whether or not the cap is sensed.
*/
MovementController.prototype.checkIfReceptionHasBottle = function() {
  var reception = this.reception;
  var deck = this.deck;
  var self = this;
  return new Promise(function (resolve) {
    deck.capCheck().then(function(firstCheck){
      if(firstCheck == "C"){
        resolve(firstCheck);
      }else{
        deck.moveToElevator().then(function(){
          reception.moveToCapperForUncapping().then(function(){
            deck.capCheck().then(function(code){
              reception.moveToDeck().then(function(){
                if(code == "C"){
                  //When we raised the bottle we see the cap
                  //So let's uncap it!
                  winston.log("debug", "uncapping Bottle");
                  self.uncapBottleAttempt().then(function(){
                    resolve(code);
                  });
                }else{
                  resolve(code);
                }
              });
            });
          });
        });
      }
    });
  });
};

/**
  Used to move the bottle under the filler on an order request

  @method moveBottleToFiller
  @return {Promise} Resolves with whether or not the bottle makes it to the filler.
*/
MovementController.prototype.moveBottleToFiller = function() {
  var reception = this.reception;
  var deck = this.deck;
  var self = this;
  winston.log("debug", "hit move bottle to filler");
  return new Promise(function (resolve, reject) {
    reception.moveToDeck().then(function(){
      resolve(deck.moveToFiller());
    });
  });
};

/**
  Used to move the bottle under the filler on an order request

  @method moveBottleToFiller
  @return {Promise} Resolves with whether or not the bottle makes it to the filler.
*/
MovementController.prototype.sanitizeProbe = function() {
  winston.log("info", "sanitizing Probe");
  var lighting = this.lighting;
  var gantry = this.gantry;
  var self = this;
  return lighting.turnOnUVLight().then(function(){
    return gantry.placeProbeInUV();
  })
};


/**
  Importantly, this method also begins the process of filling the slider
  again. This process will not be finished when the returned promise resolves.
  Therefore, we keep track of the refill promise with `bottleReadyToUncapPromise`.

  @method bottleToDelivery
  @return {Promise} Resolves when the bottle has been delivered to customer.
*/
MovementController.prototype.bottleToDelivery = function() {
  var deck = this.deck,
      reception = this.reception,
      gantry = this.gantry,
      self = this;

  var downPromise = deck.moveToElevator().then(function() {
    return self.capBottleSuccess();
  }).then(function() {
    return reception.moveToDoor();
  });


  var deliverPromise = downPromise.then(function() {
    return reception.open();
  });

  // Hold on to the previous promise. The nextBottleClosedPromise property gets over-written below.
  let nextBottleClosedPromiseForSliderLoad = this.nextBottleClosedPromise;
  var loadSliderPromise = downPromise.then(function() {
    return RSVP.all([
      deck.moveToDropOff(),
      nextBottleClosedPromiseForSliderLoad
    ]);
  }).then(function() {
    return gantry.deliverBottle().then(function(){
      self.sanitizeProbe();
      //clear the nextBottleClosedPromise
    });
  });

  var receptionCloseThenUpPromise = deliverPromise.then(function() {
    //Bottle waiting message?
    return reception.closeThenUp();
  });

  this.bottleReadyToUncapPromise = RSVP.all([
    loadSliderPromise,
    receptionCloseThenUpPromise
  ]).then(function() {
    return deck.moveToElevator().then(function(){
        return self.stageBottleForFilling().then(function() {
          return self.nextBottleClosedPromise = self.retrieveAndCloseBottleSuccess();
        });

    });
  });

  return deliverPromise;
};

/**
  Builds upon `capBottleAttempt` to check sensor and try again if capping
  was not successful.
  Check that the bottle cap was successfully put back on.
  If not, try capping again.

  @method capBottleSuccess
  @return {Promise} Resolves when bottle cap has been put back on successfully.
*/
MovementController.prototype.capBottleSuccess = function() {
  var self = this,
      deck = this.deck;

  return new RSVP.Promise(function (resolve) {
    let capAttemptCounter = 0;
    capBottleResolve(resolve); // Kick of the recursive promise

    function capBottleResolve (resolve) {
      if (capAttemptCounter >= 5) {
        // Limit the cap attempts to 5
        winston.log("debug", "MovementController Bottle Capper", {"bottleCapAttemptCount": capAttemptCounter});
        winston.log("error", "MovementController Cap Attempt Exceeded");

        return;
      }
      capAttemptCounter++;
      self.capBottleAttempt().then(function() {
        return deck.capCheck();
      }).then(function (code) {
        if (code === "N") {
          winston.log("debug", "MovementController Bottle Capper", {"bottleCapAttemptCount": capAttemptCounter});
          // No cap is in the sensor
          // Cap has been successfully placed on bottle
          resolve(capAttemptCounter);
        } else {
          // Cap is still in the sensor. Try again
          capBottleResolve(resolve);
        }
      });
    }
  });
};

/**
  Put the cap on the bottle. A call to capBottle does not
  move reception; reception must be in the "elevator" position before
  capping.
  Does not check sensor to see if capping was successful.

  @method capBottleAttempt
  @return {Promise} Resolves when done capping the bottle.
*/
MovementController.prototype.capBottleAttempt = function() {
  var deck = this.deck,
      reception = this.reception;

  return reception.moveToCapperForCapping().then(function() {
    return RSVP.all([
      deck.capBottle(),
      reception.synchronizeCapOn()
    ]);
  }).then(function() {
    return reception.moveToDeck();
  });
};

/**
  Attempt to remove the cap from the bottle. A call to uncapBottleAttempt does not
  move reception; reception must be in the "elevator" position before
  uncapping.

  @method uncapBottleAttempt
  @return {Promise} Resolves when done uncapping the bottle.
*/
MovementController.prototype.uncapBottleAttempt = function() {
  var deck = this.deck,
      reception = this.reception;

  return reception.moveToCapperForUncapping().then(function() {
    return RSVP.all([
      deck.uncapBottle(),
      reception.synchronizeCapOff()
    ]);
  }).then(function() {
    return reception.moveToDeck();
  });
};


/*
  Retrieve bottle successfully, then attempt to close the bottle flap.
  If flap close fails, throw away bottle and start the process over.
  If flap close succeeds, then the gantry has a new bottle to place in the slider.

  @method retrieveAndCloseBottleSuccess
  @return {Promise}
*/
MovementController.prototype.retrieveAndCloseBottleSuccess = function() {
  let gantry = this.gantry;

  function promiseCallback (resolve, reject) {
    gantry.retrieveNextBottleSuccess().then(function() {
      return gantry.closeBottle();
    }).then(function (code) {
      if (code === "K") {
        // Flap closed successfully
        resolve(code);
      } else { // code === "G"
        // Flap failed to close, throw away bottle
        winston.log("info", "Gantry flap close failed, throwing bottle away.");
        gantry.throwAwayBottle().then(function() {
          promiseCallback(resolve);
        });
      }
    }, function (error) {
      // Handle Gantry failed to retrieve next bottle
      if (error === gantry.ERROR.towerEmpty) {
        // Tower out of bottles
        winston.log("warn", "Tower out of bottles");
        //BLE.statusError("Kiosk out of bottles");
      } else if (error === gantry.ERROR.missAttemptExceeded) {
        // Too many misses on same bottle
        winston.log("error", "Gantry Missed Bottle Attempt Exceeded");
      } else {
        reject(error);
      }
    });
  }

  return new RSVP.Promise(promiseCallback);
};


module.exports = MovementController;
