var winston = require("winston");
var ErrorLogTransport = require("./ErrorLogTransport");
const IPCLogTransport = require("./IPCLogTransport");

// The default logger set to "silly" level
winston.level = "silly";

// Setup File
winston.add(winston.transports.File, {
  filename: "winston-logs.log"
});

// Setup ErrorLogTransport
winston.add(ErrorLogTransport);

winston.add(IPCLogTransport);
