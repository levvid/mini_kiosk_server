"use strict";

var SerialPort = require("serialport");
var DeviceMonitor = require("./DeviceMonitor");
var winston = require("winston");
const StartUpManager = require("./StartUpManager");
var postal = require("postal");
var HID = require("node-hid");

/**
  ModuleManager allows the app to register modules by an identifier and provide
  lifecycle callbacks. ModuleManager is a "static" class, there is no
  constructor.

  The class maintains a hash of registered modules and the ports matched to each
  module.

  @static
*/

// Key: module identifier. Value: object with callback functions and port
var registeredModules = {};
var modulePortCount = 0; // Track count of module ports
var deviceMonitor;
var scannerCallback;
var postalChannel = postal.channel("moduleManager");

/**
  Creates an instance of `DeviceMonitor` and hooks up callbacks to the matching
  functions in ModuleManager. Call this function to begin detecting and
  processing devices after registering modules with the ModuleManager.

  @method beginMonitoring
*/
exports.beginMonitoring = function() {
  // Create device monitor and pipe to ModuleManager when device is found
  deviceMonitor = new DeviceMonitor(this.newSerialPort, this.removeSerialPort, scannerCallback);

  // Check if the number of module ports available is greater than or equal to the number of registered modules
  var registeredModulesCount = Object.keys(registeredModules).length; // Number of registered modules
  if (modulePortCount >= registeredModulesCount) {
    // Port path count is high enough
    winston.log("verbose", "ModuleManager There are sufficient ports for number of registered modules", {registeredCount: registeredModulesCount, portCount: modulePortCount});
  }
  else {
    winston.log("warn", "ModuleManager There are fewer ports than registered modules", {registeredCount: registeredModulesCount, portCount: modulePortCount});
  }

};

/**
 * Similar behavior to the beginMonitoring function.
 * Instead of using a DeviceMonitor object, it just lists the existing ports
 * using `SerialPort.list()`.
 *
 * Used for maintenance supervisor.
 *
 * @method connectExistingPorts
 */
exports.connectExistingPorts = function() {
  SerialPort.list((error, list) => {
    if (error) {
      throw error;
    }

    // Process the existing ports
    list.forEach(port => {
      this.newSerialPort(port.comName);
    });
  });
};

/**
 * Invoke teardown method on all the existing module ports.
 *
 * Used for maintenance supervisor.
 *
 * @method disconnectExistingPorts
 */
exports.disconnectExistingPorts = function() {
  for (let key in registeredModules) {
    const moduleObject = registeredModules[key];
    this.teardownModule(moduleObject);
  }
};

/**
  Provide this function as a callback to allow ModuleManager to process new
  serial ports as they are found. If the serial port matches a registered
  module, the appropriate lifecycle callbacks will be invoked.

  @method newSerialPort
  @param {String} portPath The path to the serial port.
*/
exports.newSerialPort = function (portPath) {
  var port = new SerialPort(portPath);
  modulePortCount++;
  port.on("open", function() {
    // Then flush buffers
    port.flush(function (error) {
      if (error) {
        winston.log("error", "ModuleManager serial port flush error: "+ error);
        return;
      }
      else {
        // Then wait 400ms then send reset command
        setTimeout(function() {
          performReset(port, portPath);
        }, 400);
      }
    });
  });
};

function performReset (port, portPath) {
  // Send reset command
  port.write("!", function (error) {
    if (error) {
      winston.log("error", "ModuleManager performReset write error: "+ error);
    }
  });
  port.on("data", function (data) {
    var string = data.toString();
    for (var key in registeredModules) {
      if (string === key) { // Identify module based on key
        port.removeAllListeners("data");
        var moduleObject = registeredModules[key];
        moduleObject.port = port; // Save port
        moduleObject.portPath = portPath; // Save portPath
        moduleObject.callback(port); // Returns already open serial port
        postalChannel.publish("moduleSetup", key);
        winston.log("verbose", "Module '%s' setup complete.", key);
        moduleObject.startUpWait.done(); // Tell the StartUpManager that this wait is done
        break;
      }
    }
  });
}

/**
  Each machine module should register with the ModuleManager to have the
  appropriate lifecycle callbacks invoked. The ModuleManager will provide an
  already opened serialPort object to the callback when it is created. If the
  serialPort is disconnected, the `removeCallback` will be invoked.

  @method registerModule
  @param {String} identifier The unique module identifier.
  @param {Function} callback Callback for when the module becomes available.
  @param {SerialPort} callback.serialPort The already open serial port for the module.
  @param {Function} removeCallback Callback for when the module is disconnected.
*/
exports.registerModule = function (identifier, callback, removeCallback) {
  var startUpWait = StartUpManager.createWait("Module Manager "+ identifier);
  registeredModules[identifier] = {
    callback: callback,
    removeCallback: removeCallback,
    startUpWait: startUpWait
  };
  winston.log("verbose", "Module '%s' registered with manager.", identifier);
};

/**
  Similar to `registerModule()` this finds and opens the port for the singleton
  scanner.
  This method must be called before `beginMonitoring()`.

  @method registerScanner
  @param {Function} callback Callback for when the scanner becomes available.
  @param {SerialPort} callback.serialPort The already open serial port for the scanner.
*/
exports.registerScanner = function (callback) {
  var startUpWait = StartUpManager.createWait("Module Manager Scanner");
  var hidDevices = HID.devices();
  var deviceInfo = hidDevices.find( function(d) {
    var isScanner = d.vendorId===8208 && d.productId===30264;
    return isScanner;
  });
  if(deviceInfo) {
    var scannerDevice = new HID.HID(deviceInfo.path);
    startUpWait.done();
    callback(scannerDevice);
    winston.log("verbose", "Scanner is registered with manager.");
  }
};

/**
  Provide this function as a callback to make ModuleManager aware of serial port
  disconnects. The associated serialPort will be closed as part of cleanup.

  @method removeSerialPort
  @param {String} portPath Path to the disconnected serial port.
*/
exports.removeSerialPort = function (portPath) {
  var moduleObject;
  // Find the registered module for portPath
  for (var key in registeredModules) {
    var object = registeredModules[key];
    if (object.portPath == portPath) {
      moduleObject = object;
      break;
    }
  }
  if (moduleObject) {
    this.teardownModule(moduleObject);
    winston.log("info", "Module '%s' removal complete.", key);
  } else {
    winston.log("warn", "Did not find the disconnected port in registered modules");
  }
};

/**
 * Tear down the connection to a registered module.
 *
 * @method teardownModule
 * @param {Object} moduleObject The object in registeredModules.
 */
exports.teardownModule = function (moduleObject) {
  moduleObject.port.close(); // Close the port. Also removes event listeners.
  moduleObject.port = null;
  moduleObject.portPath = null;
  if (typeof(moduleObject.removeCallback) === "function") {
    moduleObject.removeCallback();
  }
};
