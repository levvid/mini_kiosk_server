"use strict";

const winston = require("winston");

// This transport publishes Winston log events onto the IPC message bus.
// The messages can then be subscribed from a parent process.

class IPCLogTransport extends winston.Transport {

  log (level, msg, meta, callback) {
    process.send({
      type: "winston:log",
      data: {
        level,
        msg,
        meta
      }
    }, () => {
      callback(null, true);
    });
  }

}

module.exports = IPCLogTransport;
