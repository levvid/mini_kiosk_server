"use strict";

var Udev = require("udev");
const util = require("util");
const winston = require("winston");

/**
  @class CardReader
  @extends EventEmitter
  @constructor
  @param {String} path Path for the card reader HID.
  @param {Number} productId The product ID.
*/
const DEVNAME = "DEVNAME";
const VENDOR_ID_KEY = "ID_VENDOR_ID";
const INGENICO_VENDOR_ID = "0b00"

function CardReader () {
  this.monitor = Udev.monitor();
}

/**
  Search through the connected HIDs for the card reader device.
  Create and return an instance of CardReader if the device is found.
  Returns `null` if no device is found.

  This function is synchronous.

  @method syncFindDevice
  @static
  @return {CardReader} The instance of CardReader for the found HID.
  @throws {Error} An error if a HID could not be found.
*/
CardReader.findCardReaderPath = function() {
  winston.log("debug", "findCardReaderPath called");
  return new Promise(function(resolve, reject){
    Udev.list().forEach(function (device) {
      if(!testForSerial(device)){ return; }
      var path = device[DEVNAME];
      var trimmedPath = path.replace('/dev/','');
      resolve(trimmedPath);
    }, this);
  });
};

/**
  Determine if a `udev` device is a serial port.

  @private
  @method testForSerial
  @param {Object} device The `udev` device to test.
  @return {Boolean} True if device is serial port.
*/
function testForSerial (device) {
  // Check device SUBSYSTEM is `tty`
  var subsystem = "SUBSYSTEM";
  if (!(device.hasOwnProperty(subsystem) &&
    device[subsystem] === "tty")) { return false; }
  // Check device DEVNAME starts with `/dev/tty`
  if (!(device.hasOwnProperty(DEVNAME) &&
    device[DEVNAME].indexOf("/dev/tty") === 0)) { return false; }

  if (
    !(
      device.hasOwnProperty(VENDOR_ID_KEY) &&
      checkVendorID(device[VENDOR_ID_KEY])
    )
  ) { return false; }

  // All checks have passed
  return true;
}

function checkVendorID(vendorId) {
  if (vendorId == INGENICO_VENDOR_ID) {
    // vendorId was found
    return true;
  } else {
    return false;
  }
};


module.exports = CardReader;
