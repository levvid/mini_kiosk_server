var postal = require("postal");
const util = require("util");
var winston = require("winston");
var jsonfile = require("jsonfile")
const LosantDevice = require("./LosantDevice");
var pm2 = require("pm2");

var postalChannel = postal.channel("losant-reporter");

/**
  @class StatusReporter
  @constructor
*/
var LosantReporter = function() {
  this._losantDevice = new LosantDevice();
  connectToLosant(this, this._losantDevice);


  winston.log("Moved subscribed");

  

};

function connectToLosant(self, losantDevice) {
  winston.log("debug", "connecting to losant device " + losantDevice.toString());
  // Save promise to allow testing constructor
  self._setupPromise = losantDevice.connect().then(() => {
    winston.log("debug", "connected to losant");
    // self._subscribe();  // causes an error, leave uncommented for now, TODO - Gibson
    // Send state for status start-up once the losant device connects
    losantDevice.send({
      status: "ready",
      statusDetail: "N/A"
    });
    postalChannel.publish("ready");
    winston.log("debug", "in the postal subscribe");


    postal.subscribe({
      channel: "status",
      topic: "error",
      callback: function(orderCompleteStatus) {
        winston.log("debug", "Subscribed to status channel: " + JSON.stringify(orderCompleteStatus));
        losantDevice.send({
          status: "error",
          statusDetail: "N/A"
        });
      }
    });

    postal.subscribe({
      channel: "order-server",
      topic: "orderComplete",
      callback: function(orderCompleteStatus) {
        winston.log("debug", "Subscribed to order server channel");
        if(orderCompleteStatus == "FAILED"){
          losantDevice.send({
            status: "error",
            statusDetail: "N/A"
          });
        }
      }
    });

    

    postal.subscribe({
      channel: "log-service",
      topic: "logServiceError",
      callback: function(logStatus) {
        winston.log("debug", "Subscribed to log service channel");
        losantDevice.send({
          status: "error",
          statusDetail: "N/A"
        });
      }
    });

    postal.subscribe({
      channel: "movement-controller",
      topic: "uncap",
      callback: function(logStatus) {
        winston.log("debug", "movement controller check 5555: " + logStatus);
        if(logStatus == "error"){
          losantDevice.send({
            status: "error",
            statusDetail: "Uncap attempt exceeded"
          });
        }
      }
    });

    postal.subscribe({
      channel: "movement-controller",
      topic: "closeBottle",
      callback: function(logStatus) {
        if(logStatus == "error"){
          losantDevice.send({
            status: "error",
            statusDetail: "Error closing bottle"
          });
        }
      }
    });

  }, () => {
    //MARC: Tell it to try to connect again here
    winston.log("debug", "failed to connect to Losant, restarting losant");
    losantDevice.disconnect().then(function(){
      winston.log("debug", "disconnected from Losant");
      setTimeout(function(){
        connectToLosant(self, losantDevice);
      }, 5000);
    });
    // Catch the error for a rejected connect promise
    // Don't trigger the RSVP global catch all, causing fatal-error
  });
}

LosantReporter.prototype.sendUpdate = function(){
  //Sends an update of vital information to LOSANT
  //This data is crucial to make the maintenance and mobile apps Run
  //Should be split into multiple updates for when bottles or additives are used during use
  var self = this;
  var fileData = jsonfile.readFileSync("inventory.json");

  var record = {};
  //sendbottledata
  record = {
    "leftTowerBottlesRemaining": fileData.bottles.leftTowerRemaining
  };
  this._losantDevice.send(record);

  record = {
    "rightTowerBottlesRemaining": fileData.bottles.rightTowerRemaining
  };
  this._losantDevice.send(record);

  //sendAdditiveData
  record = {};
  // Attribute prefixes are 1-indexed for more readable dashboards
  var count = 0
  fileData.additives.forEach(function(additive){
    if(additive != null){
      var prefix = "additive" + ((count + 1).toString());
      if(additive.name == "caffeine"){
        additive.name = "energy";
      }
      record[prefix + "Name"] = additive.name;
      record[prefix + "Remaining"] = additive.remaining;
      count = count + 1;
    }
  })
  this._losantDevice.send(record);
}

LosantReporter.prototype.sendPriceUpdate = function(){
  //Send Drink Pricing Info for compostable bottles
  var priceData = jsonfile.readFileSync("drinkPrice.json");
  var noFlavorPrice = priceData.dropBottle.justWaterPrice;
  var someFlavorPrice = priceData.dropBottle.flavorPrice;
  var energyShotPrice = priceData.dropBottle.supplementPrice;
  this._losantDevice.send({
    noFlavorPrice,
    someFlavorPrice,
    energyShotPrice
  });
}



module.exports = LosantReporter;
