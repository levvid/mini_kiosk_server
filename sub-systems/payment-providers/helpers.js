const postal = require("postal");
const RSVP = require("rsvp");
const winston = require("winston");

/**
  Helper methods for the Payment Provider classes.
  Allows for better sharing of code across the concrete payment providers.

  @class PaymentProvider-Helpers
  @static
*/

const postalChannel = postal.channel("payment-provider");

/**
  Publish a postal message for the "hardware.enabled" topic.

  @method publishHardwareEnabled
  @static
  @param {Function} cancelCallback Callback is invoked if payment is cancelled.
*/
exports.publishHardwareEnabled = function (cancelCallback) {
  winston.log("info", "PaymentProvider begin waiting for card swipe");
  postalChannel.publish("hardware.enabled", {
    "cancel": function() {
      // Payment cancelled
      winston.log("info", "PaymentProvider publishHardwareEnabled cancelled");
      if (typeof cancelCallback === "function") {
        cancelCallback();
      }
    }
  });
};

/**
  Publish a postal message for the "hardware.received-payment-data" topic.

  @method publishPaymentDataReceived
  @static
  @param {Function} cancelCallback Function is called if payment is cancelled.
*/
exports.publishPaymentDataReceived = function (cancelCallback) {
  winston.log("verbose", "PaymentProvider received card payment data");
  postalChannel.publish("hardware.received-payment-data", {
    "cancel": function() {
      winston.log("info", "PaymentProvider publishPaymentDataReceived cancelled");
      if (typeof cancelCallback === "function") {
        cancelCallback();
      }
    }
  });
};

/**
  Publish a postal message with the following:
  * channel: "payment-provider",
  * topic: "pre-auth.result"

  With a data object containing:
  * `success`: as given by the parameter
  * `error`: If success is false, optional error information.

  @method publishPreauthorizationResult
  @static
  @param {boolean} success The boolean value indicating whether preauthorization
  was successful, where `true` indicates success.
  @param {Error} error The optional error object if success is `false`.
*/
exports.publishPreauthorizationResult = function (success, error) {
  winston.log("info", "PaymentProvider pre-authorization result", {"success": success});
  postalChannel.publish("pre-auth.result", {
    "success": success,
    "error": error
  });
};

/**
  Provide a confirmation callback via a postal message, which determines the
  fate of the returned promise. The postal message has:
  * channel: "payment-provider",
  * topic: "vend.confirmation"

  In its data object, the "confirmed" property holds a callback function which
  expects a boolean parameter indicating whether the customer would like to
  continue with his purchase, where `true` indicates that the customer would
  like to make the purchase.
  @method receiveCustomerConfirmation
  @static
  @private
  @returns {Promise} Resolved when the confirmation callback is called with a
  truthy parameter. Rejected when the confirmation callback is called with a
  falsy parameter.
*/
exports.receiveCustomerConfirmation = function() {
  winston.log("debug", "PaymentProvider check for customer confirmation");
  return new RSVP.Promise(function (resolve, reject) {
    postalChannel.publish("vend.confirmation", {
      "confirmed": function (success) {
        winston.log("info", "PaymentProvider customer confirmation result", {"confirmed": success});
        if (success) {
          resolve();
        } else {
          reject(new Error("Payment canceled during customer confirmation."));
        }
      }
    });
  });
};
