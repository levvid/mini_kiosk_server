const Stripe = require("stripe");
const APIClient = require("../APIClient");
const CardReader = require("../CardReader");
const PaymentProviderHelpers = require("./helpers");
const DrinkPrice = require("../drink-price");
const RSVP = require("rsvp");
const winston = require("winston");
const util = require("util");
const postal = require("postal");
var jsonfile = require("jsonfile");


/**
  Provides payment processing backed by Stripe.

  This module is a singleton.

  @class StripePaymentProvider
  @static
*/

var stripeClient;
var currentChargeId;
var cardReader;

/**
  Setup up the payment provider.
  Finds and instantiates the Card Reader.

  @method init
  @static
  @param {String} stripeApiKey The Stripe API key.
  @return {StripePaymentProvider} Returns the static module.
  @throws {Error} An error with the reason the payment provider could not be set up.
*/
exports.init = function (stripeApiKey) {
  stripeClient = new Stripe(stripeApiKey);
  currentChargeId = null;
  cardReader = CardReader.syncFindDevice();
  if (!cardReader) {
    winston.log("error", "Card Reader HID not found. Please connect and launch app again.");
  }
  return exports;
};

/**
  @method preauthorizePayment
  @static
  @param {Number} cents The price of the order to pre-authorize in cents.
  @return {Promise}
  @throws {Error}
*/
exports.preauthorizePayment = function (cents) {
  if (typeof cents !== "number") {
    throw new Error("preauthorizePayment requires a numerical price in cents");
  }

  if (currentChargeId) {
    winston.log("warn", "StripePaymentProvider preauthorizePayment called while currentChargeId already exists");
  }

  // Can be used to reject the deferred cardInfoPromise
  const cardInfoDeferred = cardReader.cardInfoDeferredPromise();
  PaymentProviderHelpers.publishHardwareEnabled(function() {
    // Payment has been cancelled
    const error = new Error("Payment has been cancelled");
    error.cancelled = true;
    cardInfoDeferred.reject(error);
  });
  return tokenThenPreAuth(cardInfoDeferred.promise, cents).then(function (result) {
    PaymentProviderHelpers.publishPreauthorizationResult(true);
    currentChargeId = result.chargeId;
    // Successful preauthorization, continued
    return PaymentProviderHelpers.receiveCustomerConfirmation().catch(function (confirmationError) {
      // Failed to receive confirmation; the charge has already been
      // preauthorized so it must be voided
      return exports.voidPreauthorization().catch(function (voidError) {
        winston.log("warn", "StripePaymentProvider preauthorizePayment could not release pre-authorization after customer declined purchase", {"error": util.inspect(voidError)});
      }).finally(function() {
        // Customer declined confirmation so reject the returned promise
        // whether or not releasing the pre-authorization was successful
        throw confirmationError;
      });
    });
  }, function (error) {
    if (error.cancelled) {
    // Was cancelled by the customer
    // Do not publish pre-authorization result
    }
    else {
    // Unsuccessful preauthorization
      PaymentProviderHelpers.publishPreauthorizationResult(false, error);
    }
    throw error; // Reject the rest of the promise chain
  });
};

/**
  This function was created to reject the cardInfoDeferredPromise on an order
  made with the free drink codes
  @method rejectCardInfoDeferred
  @static
*/
exports.rejectCardInfoDeferred = function () {
  //const cardInfoDeferred = cardReader.cardInfoDeferredPromise();
  //const error = new Error("Payment has been cancelled");
  //error.cancelled = true;
  //cardInfoDeferred.reject();
  return;
};

/**
  Pass in a promise that resolves with card info.
  This function then creates a token and submits the token to the API to create
  a pre-authorization.
  The function returns a promise that resolves with the pre-authorization.

  @method tokenThenPreAuth
  @private
  @static
  @param {Promise} cardInfoPromise A promise that resolves with the card info.
  @param {Number} cents The amount in cents for which to create a pre-auth.
  @return {Promise} Resolves with the pre-authorization.
*/
function tokenThenPreAuth (cardInfoPromise, cents) {
  return cardInfoPromise.then(function (cardInfo) {
    PaymentProviderHelpers.publishPaymentDataReceived();

    // Send the cardInfo up to stripe to get a token
    return stripeClient.tokens.create({
      "card": {
        "number": cardInfo.PAN,
        "exp_month": cardInfo.expMonth,
        "exp_year": cardInfo.expYear
      }
    });
  }).then(function (token) {
    winston.log("verbose", "StripePaymentProvider token created");
    // Send the token to Heroku to create a preauthorization
    const tokenId = token.id;
    var currency = DrinkPrice.getCurrencyType();
    var associateId = getStripeAssociateId();
    return APIClient.createPreAuthorization(tokenId, cents, currency, associateId);
  });
}

/**
  Capture the current pre-authorization.

  @method settlePayment
  @static
  @return {Promise} Resolves if the payment is captured successfully.
*/
exports.settlePayment = function() {
  if (currentChargeId) {
    return APIClient.capturePreAuthorization(currentChargeId).finally(function() {
      currentChargeId = null;
    });
  }
  else {
    return RSVP.reject(new Error("StripePaymentProvider settlePayment called with no current payment"));
  }
};

/**
  Release the current pre-authorization.

  @method voidPreauthorization
  @static
  @return {Promise} Resolves if the pre-authorization is released successfully.
*/
exports.voidPreauthorization = function() {
  return checkInternet().then(function(isConnected){
    if (isConnected) {
    // connected to the internet
      if (currentChargeId) {
        return APIClient.refundCharge(currentChargeId).finally(function() {
          currentChargeId = null;
        });
      }
      else {
        return RSVP.reject(new Error("StripePaymentProvider voidPreauthorization called with no current payment"));
      }
    } else {
    // not connected to the internet
      return result(true);
    }
  });
};

//For internet disconnected situations

function checkInternet() {
  var ping = require("ping");
  var host = "google.com";

  return new Promise(function(resolve){
    try {
      ping.sys.probe(host, function(isAlive){
        return resolve(isAlive);
      });
    } catch (err){
      winston.log("debug", "internet connectivity error on ping to google", err);
      return resolve(false);
    }
  });
}

function result (succeed) {
  if (succeed) {
    return RSVP.resolve();
  }
  else {
    return RSVP.reject();
  }
}

/**
  @method preauthorizePayment_NoInternet
  @static
  @param {Number} cents The price of the order to pre-authorize in cents.
  @return {Promise}
  @throws {Error}
*/
exports.preauthorizePayment_NoInternet = function (dummy) {
  const cardInfoDeferred = cardReader.cardInfoDeferredPromise();
  PaymentProviderHelpers.publishHardwareEnabled(function() {
    // Payment has been cancelled
    const error = new Error("Payment has been cancelled");
    error.cancelled = true;
    cardInfoDeferred.reject(error);
  });

  var cardInfoPromise = cardInfoDeferred.promise;
  return cardInfoPromise.then(function (cardInfo) {
    winston.log("debug", "dummy", dummy);
    return result(true);
  });
};

/**
  Capture the current pre-authorization.

  @method settlePayment_NoInternet
  @static
  @return {Promise} Resolves if the payment is captured successfully.
*/
exports.settlePayment_NoInternet = function() {
  return result(true);
};

/**
  Get the Stripe Associate ID from the JSON
**/
function getStripeAssociateId(){
  var associateId = jsonfile.readFileSync("/home/pi/btw-node-server/AssociateId.json");
  return associateId.associateId;
}
