var RSVP = require("rsvp");
var jsonfile = require("jsonfile");
var winston = require("winston");
var postal = require("postal");
var postalChannel = postal.channel("worldnet-provider");
var pm2 = require("pm2");
var CardReader = require("../CardReader.js");
const io = require('socket.io-client');
var W3CWebSocket = require('websocket').w3cwebsocket;
var fs = require("fs");

var globalWorldnetInfo = null;
var cardReaderConnected = null;
var preauthPromiseSuccess = null;

function WorldnetPaymentProvider () {
  // Subscribe to system-wide errors and cancel current order payment if needed
  var self = this;
  postal.subscribe({
    channel: "UIServer",
    topic: "Worldnet-Message",
    callback: function (payload) {
      worldnetRequest(payload)
    }
  });
}


function result (succeed) {
  if (succeed) {
    return RSVP.resolve();
  }
  else {
    return RSVP.reject();
  }
}

WorldnetPaymentProvider.prototype.connectToCardReader = function () {
    return CardReader.findCardReaderPath().then(function(path){
      globalWorldnetInfo = jsonfile.readFileSync("worldnetInfoJSON.json");
      globalWorldnetInfo.path = path;
      winston.log("debug", globalWorldnetInfo);
      return establishConnection();
    });
}


WorldnetPaymentProvider.prototype.preauthorizePayment = function (cents) {
  console.log("Price is + " + cents);
  winston.log("debug", "Price is  " + cents);
  if(cents == 0){
    var payload = {
      type: "RES_ON_SALE_RESPONSE"
    }
    return RSVP.resolve(payload);
  }
  preauthPromiseSuccess = null;
  var self = this;
  var payload = {action: "preauthorize-request",
                 price: cents}
  worldnetRequest(payload);
  return new Promise(function(resolve, reject){
    preauthPromiseSuccess = resolve;
  });
};

WorldnetPaymentProvider.prototype.preauthorizePayment_NoInternet = function () {
  return result(this.succeed);
};

WorldnetPaymentProvider.prototype.settlePayment = function () {
  return result(this.succeed);
};

WorldnetPaymentProvider.prototype.settlePaymentRefill = function (paymentObject, refundAmount) {
  var paymentInfo = paymentObject.message;
  var payload = {
                 action: "partial-refund-request",
                 paymentObject: paymentInfo,
                 refundAmount: refundAmount
                }
  if(refundAmount != 0){
    //Don't publish unless theres actually something to refund
    worldnetRequest(payload);
  }
  return result(this.succeed);
};


WorldnetPaymentProvider.prototype.voidPreauthorization = function (paymentObject) {
  var self = this;
  var paymentInfo = paymentObject.message;
  var payload = {action: "refund-request",
                 paymentObject: paymentInfo}
  worldnetRequest(payload);
  return result(this.succeed);
};

exports.rejectCardInfoDeferred = function () {
  //Added in if someone tries to use a free code for NoOpPaymentProvider
  return;
};


//init paymentMiddleware websocket
var self = this;

function worldnetRequest(payload){
  switch(payload.action){
    case "preauthorize-request":
      winston.log("debug", "preauth request hit");
      processSale(payload.price);
      break;
    case "refund-request":
      winston.log("debug", "refund request hit");
      refundSale(payload.paymentObject);
      break;
    case "partial-refund-request":
      winston.log("debug", "partial refund hit");
      partialRefund(payload);
      break;
    case "cancel-transaction":
      cancelCurrentTransaction();
      winston.log("debug", "cancel-transaction hit");
    default:
      console.log("worldnet request payload not what I expected", payload)
  }
}

var client;

function establishConnection(){
  client = new W3CWebSocket('ws://localhost:8080/middleware');
  return new Promise(function(resolve, reject){

    
    //waits til connected to resolve the promise
    client.onopen = function() {
      winston.log("debug","connected to payment websocket");
    };
    client.onerror = function() {
      setTimeout(function(){
        resolve(establishConnection());
      },30000);
      winston.log("debug", "connection error waiting 30 seconds then trying again");
    };
    client.onmessage = function(e){
      const message = JSON.parse(e.data)
      winston.log("debug", "worldnetSocketEvent", message);
      handleIncomingMessage(message);
    };
    cardReaderConnected = resolve;
  });
}

function sendMessage(obj){
  const jsonStrToSend = JSON.stringify(obj);
  console.log("JSON Request: \n" + jsonStrToSend);
  client.send(jsonStrToSend);
}


//initialize the middleware on bootup
function connectToMiddleware(){
  var payload = {
      type: "REQ_WS_INIT",
      data: {
        mode: globalWorldnetInfo.mode,
        logLevel: "LEVEL_NONE"
      }
    };
    sendMessage(payload);
}

//initialize the middleware on bootup
function initWSMiddlewareWithConfig(){
  var payload = {
      type: "REQ_INIT_WITH_CONFIGURATION",
      data: {
        terminalId: globalWorldnetInfo.terminalId,
        secret: globalWorldnetInfo.secretKey
      }
    };
    sendMessage(payload);
}

function initCardReaderDevice(){
  var payload =
  {
      type: "REQ_INIT_DEVICE",
      data: {
        device: "INGENICO",
        connectionType: "USB",
        inputMethod: "SWIPE_OR_INSERT_OR_TAP",
        portName: globalWorldnetInfo.path,
        emvType: "STANDARD"
      }
    };
    sendMessage(payload);
}

function processSale(price){
  var priceInDollars = price / 100;
  var payload =
  {
      type: "REQ_PROCESS_SALE",
      data: {
        amount: priceInDollars,
        emvType: "QUICK_CHIP"
      }
    };
    sendMessage(payload);
}

function refundSale(paymentObject){
  console.log("paymentObject", paymentObject);
  var payload =
    {
        type: "REQ_PROCESS_REFUND",
        data: {
          uniqueRef: paymentObject.uniqueRef,
          amount: paymentObject.authorizedAmount,
          reason: "Customer did not receive item"
        }
      };
      sendMessage(payload);
}

function partialRefund(refundInfo){
  var paymentObject = refundInfo.paymentObject;
  var refundAmount = refundInfo.refundAmount;
  var payload =
    {
        type: "REQ_PROCESS_REFUND",
        data: {
          uniqueRef: paymentObject.uniqueRef,
          amount: refundAmount,
          reason: "Customer spent less than preauthorized amount"
        }
      };
      sendMessage(payload);
}

function cancelCurrentTransaction(){
  var payload =
    {
      type: "REQ_CANCEL_TRANSACTION"
    }
    sendMessage(payload);
}

function handleIncomingMessage(message){
  var type = message.type
  console.log(`Message Type: ${type}`);

  switch (type) {
    case "RES_ON_WS_INIT_REQUIRED":
        console.log("RES_ON_WS_INIT_REQUIRED: " + message.description)
        connectToMiddleware();
        break;
    case "RES_ON_WS_INIT":
        console.log("RES_ON_WS_INIT: " + message.description);
        //Response after successful init of middleware
        initWSMiddlewareWithConfig();
        break;
    case "RES_ON_WS_RESTART":
        console.log("RES_ON_WS_INIT: " + message.description);
        //response sent after successfully restarting websocket

        break;
    case "RES_ON_MESSAGE":
        console.log("RES_ON_MESSAGE: " + JSON.stringify(message.data.message));
        var payload = { type: "RES_ON_MESSAGE",
                        message: message.data.message
                      }

        postalChannel.publish("worldnet-message", payload);
        processMessage(payload);

        break;
    case "RES_ON_SET_MODE":
        console.log("RES_ON_SET_MODE: " + JSON.stringify(message.data));
        //response after setting SDK mode (unused by me)

        break;
    case "RES_ON_SET_LOG_LEVEL":
        console.log("RES_ON_SET_LOG_LEVEL: " + JSON.stringify(message.data));
        //response after setting Log level (probably not going to use)

        break;
    case "RES_ON_SETTINGS_RETRIEVED":
        console.log("RES_ON_SETTINGS_RETRIEVED: " + JSON.stringify(message.data.settings));
        initCardReaderDevice();

        break;
    case "RES_ON_DEVICE_CONNECTED":
        console.log("RES_ON_DEVICE_CONNECTED: " + JSON.stringify(message.data.deviceInfo));
        //response when a device gets connected
        var payload = {
                        type: "RES_ON_DEVICE_CONNECTED",
                        message: "CardReaderConnected"
                      }
        processMessage(payload);
        //this.wsClient.getDeviceInfo();

        break;
    case "RES_ON_SALE_RESPONSE":
        console.log("RES_ON_SALE_RESPONSE: " + JSON.stringify(message.data.saleResponse));
        var payload = { type: "RES_ON_SALE_RESPONSE",
                        message: message.data.saleResponse
                      }
        processMessage(payload);
        //sale response returns here
        //setSaleResponse && setSaleResponse(message.data.saleResponse);

        break;
    case "RES_ON_REFUND_RESPONSE":
        console.log("RES_ON_REFUND_RESPONSE: " + JSON.stringify(message.data.refundResponse));
        //refund response (likely wont use this, will use cancel isntead)
        //setRefundResponse && setRefundResponse(message.data.refundResponse);

        break;
    case "RES_ON_TRANSACTION_LIST_RESPONSE":
        console.log("RES_ON_TRANSACTION_LIST_RESPONSE: " + JSON.stringify(message.data.transactions));
        //get all of the transaction data (all of it!?)
        /*
        const normalizedTransactionData = {};

        if (message.data != null && message.data.transactions != null) {
            message.data.transactions.forEach(data => {
                const listDetails = {
                    cardNumber: data.cardNumber,
                    cardType: data.cardType,
                    amount: data.amount,
                    transactionDate: data.transactionDate,
                    transactionState: data.transactionState,
                };

                normalizedTransactionData[data.orderId] = {
                    listDetails,
                    viewDetails: { ...data },
                };
            });

        }

        setTransactionListResponse && setTransactionListResponse(normalizedTransactionData);
        */
        break;
    case "RES_ON_REQUEST_SET_AMOUNT":
        console.log("RES_ON_REQUEST_SET_AMOUNT: " + JSON.stringify(message.data.sale));

        const sale = message.data.sale;
        /*
        if (sale != null) {
            if (sale.delayedAuthEnabled) {
                // store the reponse delayed auth coresale so that we have an access to the object
                setDelayedAuthCoreSale(sale);
                // flag that we are requesting for the amount
                // on this sample app, this will cause the amount dialog to pop up
                setIsRequestingAmount(true);
            } else {
                // if delayed auth is disabled,
                // we just submit the sale
                this.wsClient.submitAmount({ sale: sale });
            }
        }
        */


        break;
    case "RES_ON_REQUEST_CLOSE_BATCH_RESPONSE":
        console.log("RES_ON_REQUEST_CLOSE_BATCH_RESPONSE");

      /*  toaster.success(`${message.description}`, {
            duration: 3
        });
        */

        break;
    case "RES_ON_CALCULATED_AMOUNT_RESPONSE":
        console.log("RES_ON_CALCULATED_AMOUNT_RESPONSE: " + JSON.stringify(message.data));

        //setCalculatedAmountResponse && setCalculatedAmountResponse(message.data);
        break;
    case "RES_ON_SELECT_APPLICATION":
        console.log("RES_ON_SELECT_APPLICATION");

        this.wsClient.submitApplication(0);

        break;
    case "RES_ON_SIGNATURE_REQUIRED":
        console.log("RES_ON_SIGNATURE_REQUIRED");
        //signature is never required for us
        //setIsRequestingSignature && setIsRequestingSignature(true);

        break;
    case "RES_ON_SUPPORTED_DEVICES_RETRIEVED":
        console.log("RES_ON_SUPPORTED_DEVICES_RETRIEVED: " + JSON.stringify(message.data));
        break;
    case "RES_ON_GET_DELAYED_AUTH_ENABLED":
        console.log("RES_ON_GET_DELAYED_AUTH_ENABLED: " + JSON.stringify(message.data));

        //setIsDelayedAuthEnabled && setIsDelayedAuthEnabled(message.data.isDelayedAuthEnabled);

        break;
    case "RES_ON_GET_POLLING_ENABLED":
        console.log("RES_ON_GET_POLLING_ENABLED: " + JSON.stringify(message.data));

        //setIsPollingEnabled && setIsPollingEnabled(message.data.isPollingEnabled);

        break;
    case "RES_ON_TERMINAL_HEALTH_LISTENER_STARTED":
        console.log("RES_ON_TERMINAL_HEALTH_LISTENER_STARTED");
        break;
    case "RES_ON_TERMINAL_HEALTH_LISTENER_STOPPED":
        console.log("RES_ON_TERMINAL_HEALTH_LISTENER_STOPPED");
        break;
    case "RES_ON_SET_OFFLINE_MODE_ENABLED":
        console.log("RES_ON_SET_OFFLINE_MODE_ENABLED - Enabled: " + message.data.enabled);
        break;
    case "RES_ON_OFFLINE_SALE_REQUEST":
        console.log("RES_ON_OFFLINE_SALE_REQUEST - " + JSON.stringify(message.data));



        break;
    case "RES_ON_DEVICE_INFO_RETURNED":
        console.log("RES_ON_DEVICE_INFO_RETURNED - " + JSON.stringify(message.data));


        break;
    case "RES_ON_WS_ERROR": // WEBSOCKET ERROR
        winston.log("debug", "RES_ON_WS_ERROR", message.description);


        break;
    case "RES_ON_DEVICE_ERROR":
        console.log("RES_ON_DEVICE_ERROR", message.data.deviceError);
        var payload = { type: "RES_ON_DEVICE_ERROR",
                        message: message.data.deviceError
                      }
        postalChannel.publish("worldnet-message", payload);
        processMessage(payload);
        break;
    case "RES_ON_TERMINAL_HEALTH_LISTENER_ERROR":
        console.log("RES_ON_TERMINAL_HEALTH_LISTENER_ERROR", message.data.error);
        /*toaster.danger(`${message.responseType} - ${message.data.error}`, {
            du*ration: 3
        });*/
        break;
    case "RES_ON_ERROR": // GoChip SDK On Error
        console.log("RES_ON_ERROR GoChip SDK on Error");
        console.log(message.data);
        var payload = {
                        type: "RES_ON_ERROR",
                        message: "GoChipSDKError"
                      }
        processMessage(payload);

        break;
  }
}

function processMessage(payload){
  //Resolve the promise
  console.log("WNPP received message back", payload);
  console.log("this is the payload type " + payload.type);
  if(payload.type == "RES_ON_SALE_RESPONSE"){
    console.log("calling preauth resolve");
    if (payload.message.code === 'A') {
      console.log("Card accepted " + payload.message.code);
    }
    else {  // D for declined
      console.log("Card declined " + payload.message.code);
    }

    preauthPromiseSuccess(payload);
    
  }
  if(payload.type == "RES_ON_ERROR"){
    //Websocket error let's try to restart it
    winston.log("debug", "Some sort of websocket error?");
    //wait 10 seconds and try again to connect to the middleware
    setTimeout(function(){
      initWSMiddlewareWithConfig();
    }, 10000);
  }
  if(payload.type == "RES_ON_DEVICE_ERROR"){ //NEED TO FIGURE OUT WHAT MESSAGE TO USE
    if(payload.message == "TRANSACTION_ERROR"){
      winston.log("debug", "res dev err trans err hit");
      //if error is TRANSACTION_ERROR there was an issue reading the card lets retry
      preauthPromiseSuccess(payload);
      console.log("device reported error reading card lets try again");
    }else{
      winston.log("debug", "res on device error else");
      preauthPromiseSuccess(payload);
      
      //if error is TRANSACTION_CANCELLED the person hit cancel on the transaction
    }
  }
  if(payload.type == "RES_ON_DEVICE_CONNECTED"){
    winston.log("debug", "card reader connected")
    cardReaderConnected();
  }
}



module.exports = WorldnetPaymentProvider;
