var RSVP = require("rsvp");
var winston = require("winston");

function NoOpPaymentProvider () {
  this.succeed = true;
}

function result (succeed) {
  if (succeed) {
    return RSVP.resolve();
  }
  else {
    return RSVP.reject();
  }
}

NoOpPaymentProvider.prototype.connectToCardReader = function () {
  return result(this.succeed);
};

NoOpPaymentProvider.prototype.preauthorizePayment = function (cents) {
  winston.log("debug", "No op preauthorizing payment", cents);
  winston.log("debug", "No op preauthorizing payment", this.succeed);
  var payload = {
    type: "RES_ON_SALE_RESPONSE"
  }
  return RSVP.resolve(payload);
};

NoOpPaymentProvider.prototype.preauthorizePayment_NoInternet = function () {
  return result(this.succeed);
};

NoOpPaymentProvider.prototype.settlePayment = function () {
  return result(this.succeed);
};

NoOpPaymentProvider.prototype.settlePaymentRefill = function (orderAmount) {
  return result(this.succeed);
};

NoOpPaymentProvider.prototype.settlePayment_NoInternet = function () {
  return result(this.succeed);
};


NoOpPaymentProvider.prototype.voidPreauthorization = function () {
  return result(this.succeed);
};

exports.rejectCardInfoDeferred = function () {
  //Added in if someone tries to use a free code for NoOpPaymentProvider
  return;
};

module.exports = NoOpPaymentProvider;
