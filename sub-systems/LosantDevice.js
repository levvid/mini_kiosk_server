"use strict";
const Device = require("losant-mqtt").Device;
const jsonfile = require("jsonfile");
const validate = require("jsonschema").validate;
const RSVP = require("rsvp");
const winston = require("winston");
const EventEmitter = require("events");
const util = require("util");

/**
 * The schema for the Losant credentials file. The required properties are:
 * - LOSANT_KEY: the API key issued by Losant's "Access Keys" form. Each key
 *   should be scoped to the device on which it is deployed.
 * - LOSANT_SECRET: The API secret issued when the key is generated
 * - LOSANT_DEVICE_ID: The Losant-assigned device ID for this kiosk
 *
 * @property FILE_SCHEMA
 * @type {Schema}
 * @private
 * @static
 * @const
*/
const FILE_SCHEMA = {
  "type": "object",
  "properties": {
    "LOSANT_KEY": {"type": "string"},
    "LOSANT_SECRET": {"type": "string"},
    "LOSANT_DEVICE_ID": {"type": "string"}
  },
  "required": ["LOSANT_KEY", "LOSANT_SECRET", "LOSANT_DEVICE_ID"]
};

class LosantDevice extends EventEmitter {

  /**
   * @class LosantDevice
   * @constructor
   * @param {String} [filename=losant-config.json] The name of the Losant
   * credentials file. The file should follow the
   * {{#crossLink "LosantDevice/FILE_SCHEMA:property"}}{{/crossLink}}.
   */
  constructor (filename) {
    super();
    var self = this;
    this.filename = filename || "losant-config.json";
    const config = jsonfile.readFileSync(this.filename);
    validate(config, FILE_SCHEMA, {throwError: true});

    this.LOSANT_SECRET = config.LOSANT_SECRET;
    this.LOSANT_KEY = config.LOSANT_KEY;
    this.LOSANT_DEVICE_ID = config.LOSANT_DEVICE_ID;

    // Create Losant Device
    const options = {
      id: this.LOSANT_DEVICE_ID,
      key: this.LOSANT_KEY,
      secret: this.LOSANT_SECRET
    };
    this._device = new Device(options);

    /**
     * Re-emit the "command" event from the underlying Device object.
     * @event command
     * @param {object} command See the Losant MQTT docs.
     */
    // Allow listening for "command" event directly on LosantDevice
    this._device.on("command", command => {
      winston.log("debug", "received losant command", command.name);
      if(command.name == "enter-maintenance-mode"){
        //Report back that it is in maintenance mode (spoofing maintenance app for backwards compatibility)
        self.send({
          status: "maintenance",
          statusDetail: "N/A"
        })
      }
      if(command.name == "exit-maintenance-mode"){
        var exec = require("child_process").exec;
        //reboots the whole machine on exit maintenance mode command
        exec("sh /usr/local/bin/reboot.sh");
      }

      //this.emit("command", command);
    });

    this._device.on("reconnect", () => {
      winston.log("info", "LosantDevice reconnect event");
    });

    this._device.on("error", error => {
      winston.log("warn", "LosantDevice error event", {error: util.inspect(error)});
    });

    this._device.on("close", () => {
      winston.log("warn", "LosantDevice close event");
    });

    this._device.on("offline", () => {
      winston.log("warn", "LosantDevice offline event");
    });
  }

  /**
   * Connect the MQTT Device to the Losant service.
   *
   * @method connect
   * @returns {Promise} Resolves on 'connect' event, and rejects if there is an error.
   */
  connect() {
    winston.log("debug", "in losantDevice.connect()");
    return new RSVP.Promise((resolve, reject) => {
      // winston.log("warn", "losant device check----- " + this._device.LOSANT_DEVICE_ID);
      this._device.connect(error => {
        if (error) {
          winston.log("warn", "LosantDevice connection error: "+ error);
          reject(error);
          return;
        }
        resolve();
      });
    });
  }

  /**
   * Send device state to the Losant service for this Device.
   *
   * @method send
   * @param {Object} state The key-value object to send to Losant. Each key must
   * correspond to a device "attribute" as configured on Losant, and each value
   * must have the type specified on Losant for its corresponding key.
   * @returns {Promise} Resolves when sending is complete. Rejects if there is
   * an error sending the state.
   */
  send (state) {
    return new RSVP.Promise((resolve, reject) => {
      this._device.sendState(state, error => {
        if (error) {
          reject(error);
        } else {
          resolve();
        }
      });
    });
  }

  /**
   * Disconnect the MQTT Device from the Losant service.
   *
   * @method disconnect
   * @returns {Promise} Resolves on disconnection.
   */
  disconnect() {
    return new RSVP.Promise(resolve => {
      this._device.disconnect(() => {
        winston.log("debug", "Losant Device disconnected");
        resolve();
      });
    });
  }

}

module.exports = LosantDevice;
