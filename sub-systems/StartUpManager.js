"use strict";

const RSVP = require("rsvp");
const winston = require("winston");

/**
  The StartUpManager singelton is responsible for waiting on all dependencies
  before starting normal operation.
  @class StartUpManager
  @static
*/

var promises = {};

/**
  Create a wait to be registered with the StartUpManager. The function returns
  a wrapper object with a `done` function. Invoke the `done` function when
  the wait is ready.

  Wait names must be unique keys.
  @method createWait
  @static
  @param {String} name The unique name of the wait.
  @return {Object} A wrapper object with a `done` function.
*/
exports.createWait = function (name) {
  var wait = {};
  var promise = new RSVP.Promise(function (resolve) {
    wait.done = resolve;
  });
  promises[name] = promise;
  return wait;
};

/**
  Returns a promise that resolves when all the registered waits have been
  resolved.
  @method allResolved
  @static
  @return {Promise} The promise for all registered waits.
*/
exports.allResolved = function() {
  winston.log("verbose", "StartUpManager number of waits", {waitsCount: Object.keys(promises).length});

  var result = RSVP.hash(promises);

  startInterval();

  result.then(function() {
    // Stop timer checking if everything is ready
    stopInterval();
    winston.log("info", "StartUpManager all ready.");
  }, function (error) {
    winston.log("error", "StartUpManager: one of the waits failed", {error: error});
  });

  return result;
};

var interval;
var lastKey;

function startInterval() {
  interval = setInterval(function() {
    // Look at the promises and say what is not resolved
    for (var key in promises) {

      let promise = promises[key];
      if (promise._state === undefined) {
        // The promise has not been fulfilled
        if (lastKey === key) {
          // Already said we were waiting for this key. Don't log it again.
        } else {
          winston.log("info", "StartUpManager still has unresolved waits", {waitName: key});
          lastKey = key;
        }
        return; // Don't check for more unfulfilled promises
      }

    }
  }, 1000); // Fire every 1 second
}

function stopInterval() {
  clearInterval(interval);
  interval = null;
}
