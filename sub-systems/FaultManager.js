
var jsonfile = require("jsonfile");
var winston = require("winston");


module.exports.addActiveFault = function(system, errorDescription, errorSeverity){
  var faultObject = {
    system: system,
    error: errorDescription,
    severity: errorSeverity
  }

  var fileData = jsonfile.readFileSync("activeFaults.json");
  fileData.activeFaults.push(faultObject);
  jsonfile.writeFileSync("activeFaults.json", fileData, {spaces: 2});
};

module.exports.clearActiveFaults = function(system){
  //Clears all active faults for the specific system.
  var fileData = jsonfile.readFileSync("activeFaults.json");
  var newFaultArray = []
  winston.log("debug", fileData)
  for(fault in fileData.activeFaults){
  //  winston.log("debug", fault);
  //  winston.log("debug", fileData.activeFaults);
  //  winston.log("debug", fileData.activeFaults[fault]);
    if(fileData.activeFaults[fault].system == system){
      //Cleared this system fault do not keep active
    }else{
      //Did not clear this fault, keep it active
      newFaultArray.push(fileData[fault]);
    }
  }
  fileData.activeFaults = newFaultArray;
  jsonfile.writeFileSync("activeFaults.json", fileData, {spaces: 2});

};

module.exports.getActiveFaults = function(){
  //returns the active fault data
  var fileData = jsonfile.readFileSync("activeFaults.json");
  return fileData.activeFaults;
};
