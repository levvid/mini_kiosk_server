"use strict";

var util = require("util");
var postal = require("postal");
var CryptoMethods = require("./CryptoMethods");
var APIClient = require("./APIClient");
var winston = require("winston");
const jsonfile = require("jsonfile");
var inventoryPersistence = require("./InventoryPersistence.js");
var dataReporter = require("./DataReporter.js")

var postalChannel = postal.channel("order-server");
var cancelationCallback;
/**
  The OrderServer is responsible for processing data published by the available
  ordering sources. If the data represents a valid order, it creates an Order
  object and emits a `new` event with the order.
  @class OrderServer
  @param {PaymentProvider} paymentProvider The initialized payment provider
  module, with the following callbacks:
  * `preauthorizePayment`
  * `settlePayment`
  * `voidPreauthorization`
  @constructor
*/
/**
  The OrderServer instance emits a `new` event with the new Order object.
  @event new
  @param {Order} order The new order object.
  @param {String} source The originating source of the order.
*/
function OrderServer(paymentProvider, movementController, drinkController) {
  this.moduleName = "OrderServer";
  this.paymentProvider = paymentProvider;
  this.orderInProgress = null; // Keep track of whether there is a payment for the current order
  this.temporaryOrderHolder = null;
  this.orderInternetConnected = null;
  this.movementController = movementController;
  this.drinkController = drinkController;
  this.refillEndFromTouchscreenPromise = null
  this.refillOrderPrice = 0;
  this.refillOrderRunningPrice = 0;
  this.refillOuncesDispensed = 0;


  var self = this;

  // Subscribe to data messages from Touch Screen
  postal.subscribe({
    channel: "UIServer",
    topic: "new",
    callback: function (orderObject) {
      winston.log("debug", orderObject)
      if(orderObject.dropBottle == true){
        self._onTouchScreenCompostableOrder(orderObject);
      }else{
        winston.log("debug", "enter refill order");
        self._onTouchScreenRefillOrder(orderObject);
      }
    }
  });

  // Subscribe to data messages from Touch Screen
  postal.subscribe({
    channel: "scanner",
    topic: "data",
    callback: function (orderObject) {
      winston.log("debug", orderObject)
        self._onScannerOrder(orderObject);
    }
  });

  // Subscribe to data messages from Touch screen server
  postal.subscribe({
    channel: "UIServer",
    topic: "cancelCurrentOrder",
    callback: function () {
      self._onTouchScreenCancelOrder()
    }
  });

  // Subscribe to completed orders to finalize their payment if needed
  postal.subscribe({
    channel: "order",
    topic: "complete",
    callback: function () {
      self._onOrderComplete();
    }
  });

  // Subscribe to completed orders to finalize their payment if needed
  postal.subscribe({
    channel: "UIServer",
    topic: "refillDispenseCompleted",
    callback: function () {
      self.refillEndFromTouchscreenPromise();
    }
  });

  postal.subscribe({
    channel: "drinkController",
    topic: "refillInactivityTimeoutHit",
    callback: function () {
      self.refillEndFromTouchscreenPromise();
    }
  });

  // Subscribe to completed orders to finalize their payment if needed
  postal.subscribe({
    channel: "drinkController",
    topic: "refillData",
    callback: function (ouncesDispensed) {
      self.refillOuncesDispensed = ouncesDispensed;
      self.refillOrderRunningPrice = (self.refillOrderPrice / 16) * ouncesDispensed;
      if(self.refillOrderRunningPrice < 100){
        //if it's less than $1 add a processing fee
        if(self.refillOrderPrice == 0){
          self.refillOrderRunningPrice = 0;
        }else{
          self.refillOrderRunningPrice = self.refillOrderRunningPrice + 15;
        }
      }
      winston.log("debug", "currentPrice", self.refillOrderRunningPrice);
      var roundedOuncesDispensed = ouncesDispensed.toFixed(2);
      var payload = {refillBasePrice: self.refillOrderPrice,
                     ouncesDispensed: roundedOuncesDispensed.toString(),
                     runningPrice: self.refillOrderRunningPrice
      }
      postalChannel.publish("refillData", payload)
    }
  });

  // Subscribe to system-wide errors and cancel current order payment if needed
  postal.subscribe({
    channel: "status",
    topic: "error",
    callback: function () {
      //self._onError();
    }
  });
}

OrderServer.prototype._onScannerOrder = function (data) {
  var orderData = OrderServer.processScanData(data);
  winston.log("debug", "scanned order processed", orderData);
  var self = this;
  if(self.orderInProgress){
    return;
  }
  self.orderInProgress = {
    paymentInProgress: true
  };
    if(orderData.verifiedKiosk && orderData.verifiedSignature){
      winston.log("debug", "scanned order went through");
      postalChannel.publish("scanned-order", orderData.order);
      self._makeDrink(orderData.order).then(function(){
        winston.log("info", "orderComplete");
        postalChannel.publish("orderComplete", "success");
        self.orderInProgress = null;
      }).catch(function(error){
        //drink failed to be served
        winston.log("error", error);
        winston.log("info", "scanned drink failed");
        postalChannel.publish("orderComplete", "FAILED");
        self.orderInProgress = null;
      });
    }else{
        //The code was either for the wrong kiosk or invalid
        postalChannel.publish("scanned-order", "invalid");
        self.orderInProgress = null;
    }
};

//This method is used when the UI cancels the order
OrderServer.prototype._onTouchScreenCancelOrder = function () {
  this.orderInProgress = null;
}

//This method is called when someone orders a refill via the touch screen
OrderServer.prototype._onTouchScreenRefillOrder = function (orderData) {
  this.refillEndFromTouchscreenPromise = null;
  winston.log("verbose", "OrderServer Order received from touch screen");
  var self = this;
  if(self.orderInProgress){
    return;
  }
  self.orderInProgress = {
    paymentInProgress: true
  };
  var preauthAmount;
  if(orderData.orderPrice == 0){
    //if the price is zero we dont need to preauth for anything
    preauthAmount = 0;
  }else{
    //Setting preauthorize amount to $5.00 which should be the most anyone would spend
    preauthAmount = 500; //500 cents = $5
  }
  this.paymentProvider.preauthorizePayment(preauthAmount).then(function(paymentObject){
    self.paymentObject = paymentObject;
    self.refillOrderPrice = orderData.orderPrice;
    if(paymentObject.type == "RES_ON_SALE_RESPONSE"){
      winston.log("debug", "preauth went through");
      
      if (paymentObject.message.code == "A") {  // transaction accepted - make drink
        postalChannel.publish("payment", "success");
        self.drinkController.prepareForRefill(orderData).then(function(){
          self.refillingBottleInProgress().then(function(){
            dataReporter.postOrderData(self.refillOrderRunningPrice, orderData, self.refillOuncesDispensed, true);
            postalChannel.publish("shutoffRefill");
            winston.log("info", "orderComplete");
            inventoryPersistence.refillAdditivesDispensed(orderData, self.refillOuncesDispensed);
            postalChannel.publish("orderComplete", "success");
            var refundAmount = preauthAmount - self.refillOrderRunningPrice; //Refund the difference in the preauth amount and the amount spent
            refundAmount = (refundAmount/100).toFixed(2);
            self.paymentProvider.settlePaymentRefill(paymentObject, refundAmount);
            self.paymentObject = null;
            self.orderInProgress = null;
          });
        }).catch(function(error){
          //drink failed to be served
          winston.log("error", error);
          winston.log("info", "refill drink failed, going to issue refund");
          postalChannel.publish("orderComplete", "failure");
          self.orderInProgress = null;
        });
      }
      else {  // code == "D" card declined 
        postalChannel.publish("payment", false);
        // try again
        self.orderInProgress = null;
        winston.log("debug", "Card got declined refill order");
        // self._onTouchScreenRefillOrder(orderData);
      }
    }else{
      if(paymentObject.message == "TRANSACTION_ERROR" || paymentObject.message == "TRANSACTION_IN_PROGRESS"){
        //some sort of transaction error lets try it again
        self.orderInProgress = null;
        self._onTouchScreenRefillOrder(orderData);
      }else{
        //paymentObject.message == "DECLINED" for declined card
        //user cancelled or something that im currently not handling
        postalChannel.publish("orderComplete", "CANCELLED");
        self.orderInProgress = null;
      }
    }
  }).catch(function(error){
    //Drink Failed to be made
    winston.log("debug", "failure", error)
    postalChannel.publish("orderComplete", "FAILED");
    self.orderInProgress = null;
    self._onError(error, paymentObject);
    //Send something to the UI screen
  });
};

OrderServer.prototype._onTouchScreenCompostableOrder = function (orderData) {
  winston.log("verbose", "OrderServer Order received from touch screen");
  var self = this;
  if(self.orderInProgress){
    return;
  }
  self.orderInProgress = {
    paymentInProgress: true
  };
  this.paymentProvider.preauthorizePayment(orderData.orderPrice).then(function(payload){
    winston.log("debug", "preauth payload", payload);
    if(payload.type == "RES_ON_SALE_RESPONSE"){
      winston.log("debug", "preauth went through compostable order");

      if (payload.message.code == "A"){
        postalChannel.publish("payment", "success");
        self._makeDrink(orderData).then(function(){
          winston.log("info", "orderComplete");
          dataReporter.postOrderData(orderData.orderPrice, orderData, 0,true);  // use self.refillOuncesDispensed for this??? test first
          postalChannel.publish("orderComplete", "success");
          self.orderInProgress = null;
          self.paymentProvider.settlePayment();
        }).catch(function(error){
          //drink failed to be served
          winston.log("error", error);
          winston.log("info", "drink failed, going to issue refund");
          postalChannel.publish("orderComplete", "FAILED");
          self._onError(error, payload);
        });
      } else { // card declined
        postalChannel.publish("payment", false);
        self.orderInProgress = null;
        console.log("info", "Card got declined compostable order");
        // self._onTouchScreenCompostableOrder(orderData);
      }
    }else{

      if(payload.message == "TRANSACTION_ERROR"){
        //some sort of transaction error lets try it again
        self.orderInProgress = null;
        self._onTouchScreenCompostableOrder(orderData);
      }else{
        //payload.message == "DECLINED" for declined card
        //user cancelled or something that im currently not handling
        postalChannel.publish("orderComplete", "CANCELLED");
        self.orderInProgress = null;
      }
    }

  }).catch(function(error){
    //preauthorization error
    postalChannel.publish("payment", false);
    self.orderInProgress = null;
    winston.log("info", "preauth failed for order");
    //Send something to the UI screen
  });

};

OrderServer.prototype.refillingBottleInProgress = function(){
  var self = this;
  return new Promise(function(resolve, reject){
    self.refillEndFromTouchscreenPromise = resolve;
  });
}

/**
  function that actually controls the making of the drink
  tells the movement controller and drink controllers what to do

*/
OrderServer.prototype._makeDrink = function(orderData){
  winston.log("debug", "hit make drink", orderData);
  var self = this;
  return this.movementController.moveBottleToFiller().then(function(){
    return self.drinkController.fill(orderData).then(function(){
      winston.log("debug", "cleared drink controller");
      return self.movementController.bottleToDelivery()
    });
  });
}


OrderServer.prototype._onOrderComplete = function () {
  this.temporaryOrderHolder = null;
  var self = this;
  if (this.orderInProgress) {
    if (this.orderInProgress.paymentInProgress) {
      winston.log("info", "OrderServer settling payment for completed order", {"moduleName": this.moduleName});
      //one last internet check to make sure it hasnt dropped
          return self.paymentProvider.settlePayment().finally(function () {
            self.orderInProgress = null;
            self.temporaryOrderHolder = null;
          });
    }else{
      //payment not in progress because drink was free
      winston.log("info", "on order complete called after scanned drink processed");
      postalChannel.publish("payment-failure", {"error": {"error": "free drink processed and dispensed"}});
      if (typeof cancelationCallback === "function") {
        cancelationCallback(true);
      }
      cancelationCallback = null;
      self.orderInProgress = null;
      self.temporaryOrderHolder = null;
    }
  } else {
    winston.log("debug", "OrderServer_onOrderComplete called with no order in progress?");
    postalChannel.publish("payment-failure", {"error": {"error": "free drink processed and dispensed"}});
    self.orderInProgress = null;
    self.temporaryOrderHolder = null;
    //winston.log("error", "OrderServer _onOrderComplete called with no order in progress", {"moduleName": this.moduleName});
  }
};

OrderServer.prototype._onError = function (error, paymentObject) {
  if (this.orderInProgress && this.orderInProgress.paymentInProgress && !this.orderInProgress.paymentVoid) {
    winston.log("warn", "OrderServer voiding preauthorization for failed order");
    // Mark the orderInProgress as payment voided
    this.orderInProgress.paymentVoid = true;
    var self = this;
    // Return the promise for testing
    self.orderInProgress = null;
    self.temporaryOrderHolder = null;
    return this.paymentProvider.voidPreauthorization(paymentObject);
  }
};

/**
  Process the raw buffer data from a scan. If all checks pass, then
  the data can be created into a valid Order.

  The function returns an object with the following properties:
  * `order`: An `Order` instance or null if an Order can not be created from data.
  * `hasSignature`: `Bool` true if the order data has a signature key.
  * `verifiedSignature`: `Bool` true if the signature key verified successfully.

  @method processScanData
  @static
  @param {Buffer} data
  @return {Object} See method description for properties of the object.
*/
const ORDER_BUFFER_KEY = "orderBuffer";
OrderServer.processScanData = function (data) {
  var result = {
    order: null,
    hasSignature: false,
    verifiedSignature: false,
    verifiedKiosk: false
  };
  // Parse the data of the scan object
  const scanString = data.toString();
  var scanObject;
  try {
    scanObject = JSON.parse(scanString);
  }
  catch (error) {
    winston.log("warn", "OrderServer JSON parse error", {error: error, json: scanString});
    return result;
  }
  winston.log("info", "OrderServer process scan data", {"scanObject": scanObject});

  // Check that there is an "orderBuffer" property and its value is a string
  if (! (scanObject[ORDER_BUFFER_KEY] && typeof scanObject[ORDER_BUFFER_KEY] === "string")) {
    winston.log("warn", "OrderServer scan object did not have valid structure.", {object: scanObject});
    winston.log("info", "scan object", scanObject);
    result = scanObject;
    return result;
  }

  // Now parse the data of the order from within the scan object
  const orderData = Buffer.from(scanObject[ORDER_BUFFER_KEY], "base64");
  const orderDataString = orderData.toString();
  var orderObject;
  try {
    orderObject = JSON.parse(orderDataString);
  }
  catch (error) {
    winston.log("warn", "OrderServer JSON parse error", {error: error, json: orderDataString});
    return result;
  }

  winston.log("debug", "orderObject", orderObject);

  //marc
  winston.log("debug", "orderObject additives", orderObject.additives);
  winston.log("debug", "orderObject temp", orderObject.temperature);

  var orderOutput = {dropBottle: true,
							 flavor: "water",
						   caffeine: 0,
						   temperature: "room",
						 	 temperatureNumber: 8,
						 	 orderPrice: 0};

  Object.keys(orderObject.additives).forEach(function(additive){
    winston.log("debug", "additive", additive);
    if(additive == "energy"){
      orderOutput.caffeine = orderObject.additives[additive];
    }else{
      orderOutput.flavor = additive;
    }
  });

  if(orderObject.temperature == "room"){
    orderOutput.temperature = room;
    orderOutput.temperatureNumber = 8;
  }else{
    orderOutput.temperature = "cold";
    orderOutput.temperatureNumber = 0;
  }


  // Create an Order and add to the result
  //Defaults to dropBottle being true
  result.order = orderOutput;

  result.hasSignature = hasSignature(scanObject);

  result.verifiedKiosk = checkKioskName(scanObject);
  //check to see if the scan code is for the correct kiosk
  if(result.verifiedKiosk){
    if (result.hasSignature) {
      // If has signature, also try to verify the signature
      const verified = verifyOrderSignature(scanObject);
      result.verifiedSignature = verified;
      if (verified) {
        winston.log("verbose", "OrderServer digital signature passed verification.");
      }
      else {
        winston.log("warn", "Order digital signature failed verification.");
        postalChannel.publish("alert", {"message": "Code not valid. Please try a different code."});
      }
    }
  }else{
    winston.log("warn", "attempted to use code for different Kiosk");
    postalChannel.publish("alert", {"message": "Code not valid for this kiosk."});
  }
  return result;
};

/**
  Check for the presence of the signature key, and return `true` if and only if
  it exists and is a string.

  @method hasSignature
  @static
  @private
  @param {Object} obj The object to check for signature
  @returns {Bool} The object has a signature
*/
const SIGNATURE_KEY = "signature";
function hasSignature (obj) {
  const signature = obj[SIGNATURE_KEY];
  // Value must be a string
  return typeof signature === "string";
}

const KIOSK_KEY = "kioskId";
function checkKioskName(obj) {
  const kiosk = obj[KIOSK_KEY];
  const config = jsonfile.readFileSync("losant-config.json");
  winston.log("debug", "kioskIDfromScan", kiosk);
  winston.log("debug", "kioskIDfromLosant", config.LOSANT_DEVICE_ID);
  if(kiosk == config.LOSANT_DEVICE_ID){
    return true;
  }else{
    return false;
  }
}

const MAX_SIGNATURES_LENGTH = 100; // Limit signatures array length
var verifiedSignatures = [];
function verifyOrderSignature (json) {
  var signature = json[SIGNATURE_KEY];
  if (!hasSignature(json)) {
    winston.log("warn", "No digital signature found.");
    return false;
  }
  // Check if signature has already been verified
  if (verifiedSignatures.indexOf(signature) !== -1) {
    winston.log("warn", "Digital signature already used.");
    return false;
  }

  const orderBuffer = Buffer.from(json[ORDER_BUFFER_KEY], "base64");
  const signatureBuffer = Buffer.from(signature, "base64");
  var verified = CryptoMethods.verifyMessage(orderBuffer, signatureBuffer);
  if (verified) {
    // Add to list of verified signatures
    verifiedSignatures.push(signature);
    APIClient.sendRedeem(signature);
    // Make sure that the array has not exceeded the max
    while (verifiedSignatures.length > MAX_SIGNATURES_LENGTH) {
      // Get rid of the oldest signatures
      verifiedSignatures.shift();
    }
  }
  return verified;
}

//This function is used to cancel everything that needs
//to be cancelled for the freeDrinkRedeemer
const paymentPostalChannel = postal.channel("payment-provider");
paymentPostalChannel.subscribe("hardware.enabled", function (data) {
  // Keep reference to the cancel function wrapped in confirmation function
  var cancelPayment = data.cancel;
  cancelationCallback = function (confirm) {
    if (confirm) {
      // Cancel the payment
      cancelPayment();
    }
  };
});

function checkInternet() {
  var ping = require("ping");
  var host = "google.com";
  var cfg = {
    timeout: 6
  };

  return new Promise(function(resolve){
    try {
      ping.sys.probe(host, function(isAlive){
        return resolve(isAlive);
      }, cfg);
    } catch (err){
      winston.log("debug", "internet connectivity error on ping to google", err);
      return resolve(false);
    }
  });
}

function incrementNoInternetPreauthorizeCount(){
  var fileData = jsonfile.readFileSync("NoInternetPreauthCount.json");
  var count = fileData.count;
  count = count + 1;
  fileData.count = count;
  jsonfile.writeFileSync("NoInternetPreauthCount.json", fileData, {spaces: 2});
  return;
}

module.exports = OrderServer;
