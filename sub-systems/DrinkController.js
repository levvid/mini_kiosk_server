var RSVP = require("rsvp");
const winston = require("winston");
var postal = require("postal");
var jsonfile = require("jsonfile");

var postalChannel = postal.channel("drinkController");


/**
  Wraps the `Additive` and `Deck` modules into a single `DrinkController`
  responsible for filling a drink with an order.

  @class DrinkController
  @constructor
  @param {Additive} additive The additive module.
  @param {Deck} deck The deck module.
*/
function DrinkController (additive, deck, refillAdditive) {
  this.additive = additive;
  this.deck = deck;
  this.refillAdditive = refillAdditive;
  this.currentOrder = null;
  this._fillTimeout = 20; // Default fill timeout of 20 seconds
}

/**
  Checks with `Additive` module to see if there are enough additives available
  for the requested order.
  If so, fills the drink specified by the order.

  @method fill
  @param {Order} order
  @return {Promise} Resolves when drink is filled.
*/
DrinkController.prototype.fill = function (order) {
  var self = this;
  if (this.currentOrder) {
    return RSVP.reject("DrinkController already has currentOrder.");
  }
  var self = this;
  return this.deck.capCheck().then(function(result){
    if(result == "N"){
      //If there is no cap in the capper, throw an error
      var error = new Error("Attempted to fill with cap on bottle");
      throw error;
    }else{
      //If there is a cap in the capper, procede as normal
      // Return Promise that resolves when filling is complete
      self.refillAdditive.turnOffRefill();
      self.currentOrder = order;
      winston.log("debug", "drink controller order", order);
      var additivePromise = self.additive.sendOrder(order.flavor, order.caffeine);
      var fillerPromise = self.deck.sendOrder(order.temperature);
      const allFillingPromise = RSVP.all([additivePromise, fillerPromise]);

      var timeoutHandle;
      const timeoutPromise = new RSVP.Promise((resolve, reject) => {
        // Reject the Promise after _fillTimeout seconds
        timeoutHandle = setTimeout(() => {
          winston.log("error", "DrinkController timeout during filling");
          reject(new Error("DrinkController timeout during filling"));
        }, self._fillTimeout * 1000); // Convert seconds to milliseconds
      });

      allFillingPromise.then(() => {
        // If the filling completes, cancel the timeout
        winston.log("verbose", "DrinkController clear filling timeout");
        self.currentOrder = null;
        clearTimeout(timeoutHandle);
      });

      return RSVP.race([allFillingPromise, timeoutPromise]).then(function(){
        return self.refillAdditive.setDefaultRefill();
      })
    }
  });
};

DrinkController.prototype.prepareForRefill = function (order) {
  var self = this;
  winston.log("debug"," prepare for refill");
  var flavorColor = "B";
  var additiveLookup = jsonfile.readFileSync("additiveLookup.json");
  additiveLookup.additives.forEach(function(additive){
    if(additive.name == order.flavor){
      flavorColor = additive.color;
      return;
    }
  })
  return self.refillAdditive.enableStillWater().then(function(){
    return self.refillAdditive.checkRefillEnabled().then(function(){
      return self.refillAdditive.setLightColor(flavorColor).then(function(){
        return self.refillAdditive.sendRefillTemperature(order.temperature).then(function(){
          return self.refillAdditive.sendRefillOrder(order).then(function(){
            self.monitorRefill();
          });
        })
      });
    });
  });
}

DrinkController.prototype.monitorRefill = function(){
  var self = this;
  return new Promise(function(resolve){
    var commandInProgress = false;
    var commandQueue = [];
    var fillCheckTimeout = null;
    var inactivityTimeout = setTimeout(function(){
      winston.log("debug", "shutoffRefill due to inactivty");
      postalChannel.publish("refillInactivityTimeoutHit");
    }, 60000);

    function callCorrectCommand(commandKey){
      switch(commandKey){
        case "startDispense":
          return self.refillAdditive.startDispense();
          clearTimeout(inactivityTimeout);
          break;
        case "endDispense":
          clearTimeout(inactivityTimeout);
          inactivityTimeout = setTimeout(function(){
            postalChannel.publish("refillInactivityTimeoutHit");
            winston.log("debug", "shutoffRefill due to inactivty");
          }, 30000);
          return self.refillAdditive.endDispense();
          break;
        case "shutoffRefill":
          winston.log("debug", "shutoffRefill hit");
          clearTimeout(inactivityTimeout);
          dispenseStartPostal.unsubscribe();
          dispenseEndPostal.unsubscribe();
          shutoffRefillPostal.unsubscribe();
          return self.refillAdditive.setDefaultRefill().then(function(){
            resolve();
          })
          break;
        case "checkFlow":
          return self.refillAdditive.checkDispenseStatus().then(function(dispenseData){
            if(dispenseData == "K"){
              return;
            }else{
              var dispensedmL = parseInt(dispenseData);
              var dispensedOunces = dispensedmL * 0.0338;
              postalChannel.publish("refillData", dispensedOunces);
            }
          });
          break;
        default:
          return RSVP.reject();
      }
    }

    function fillCheck(){
      fillCheckTimeout = setTimeout(function(){
        processDispenseCommand("checkFlow")
        fillCheck();
      }, 1000);
    }

    fillCheck();

    function processDispenseCommand(command){
      commandQueue.push(command);
      if(commandInProgress == false){
        iterateThroughCommands();
        commandInProgress = true;
      }
    }

    function iterateThroughCommands(){
      if(commandQueue.length >= 1){
        //There are still commands on the queue
        callCorrectCommand(commandQueue[0]).then(function(data){
          commandQueue.shift();
          iterateThroughCommands();
        })
      }else{
        //The command queue is empty
        commandInProgress = false;
      }
    }

    var dispenseStartPostal = postal.subscribe({
      channel: "UIServer",
      topic: "refillDispenseStart",
      callback: function() {
        processDispenseCommand("startDispense");
      }
    });
    var dispenseEndPostal = postal.subscribe({
      channel: "UIServer",
      topic: "refillDispenseEnd",
      callback: function() {
        processDispenseCommand("endDispense");
      }
    });

    var shutoffRefillPostal = postal.subscribe({
      channel: "order-server",
      topic: "shutoffRefill",
      callback: function() {
        clearTimeout(fillCheckTimeout);
        processDispenseCommand("shutoffRefill");
      }
    });
  });
}

module.exports = DrinkController;
