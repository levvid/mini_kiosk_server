"use strict";

var request = require("request");
const RSVP = require("rsvp");
var winston = require("winston");
var os = require("os");
var util = require("util");

const HOST_NAME = os.hostname();

//const API_URL = "https://dropwater.herokuapp.com";
//Uncomment below line for development server
const API_URL = "https://dropwater-development-server.herokuapp.com";


const AUTHORIZATION_KEY = "A1taHaCGdgHBb3ascMJDSCtXOc1tnuW";

/**
  This static module provides methods to create API requests.

  @class APIClient
  @static
*/

/**
  Call the API to create a redeem record with the signature parameter.

  @method sendRedeem
  @static
  @param {String} signature The signature for the order that was redeemed.
*/
module.exports.sendRedeem = function (signature) {
  let options = {
    method: "POST",
    url: API_URL + "/kiosk/redeem",
    headers: {
      "Authorization": AUTHORIZATION_KEY
    },
    body: {
      signature: signature,
      kioskId: HOST_NAME
    },
    json: true
  };

  request(options, function (error, response) {
    if (response.statusCode === 200) {
      winston.log("verbose", "APIClient sendRedeem succeeded");
    } else {
      winston.log("error", "APIClient sendRedeem failed", {error: util.inspect(error), responseObject: util.inspect(response)});
    }
  });
};

/**
  This method makes a network request to create a preauthorization.
  Provide the Stripe token and price in cents as parameters.
  The Stripe token represents the card that will be preauthorized, and cents
  is the price of the order.

  On success, the Promise resolves an object with these properties:
  - `chargeId`: The id for the charge, used to capture it later.
  - `captured`: If the charge has been captured. Expected to be `false` at this point.

  @method createPreAuthorization
  @static
  @param {String} token The Stripe token for the card to preauthorize.
  @param {Number} cents The price in cents to preauthorize on the card.
  @return {Promise} Resolves if preauthorization is successful.
*/
exports.createPreAuthorization = function (token, cents, currency, associateId) {
  const options = {
    method: "POST",
    url: API_URL + "/kiosk/stripe/preAuthorization",
    headers: {
      "Authorization": AUTHORIZATION_KEY
    },
    body: {
      "token": token,
      "cents": cents,
      "kioskId": HOST_NAME,
      "currency": currency,
      "associateId": associateId
    },
    json: true
  };

  return createPromiseForRequestOptions(options);
};

/**
  Capture an existing charge pre-authorization by its ID.

  @method capturePreAuthorization
  @static
  @param {String} chargeId The pre-authorization charge ID.
  @return {Promise} Resolves if capture is successful.
*/
exports.capturePreAuthorization = function (chargeId) {
  const options = {
    method: "POST",
    url: API_URL + "/kiosk/stripe/capture",
    headers: {
      "Authorization": AUTHORIZATION_KEY
    },
    body: {
      "chargeId": chargeId
    },
    json: true
  };

  return createPromiseForRequestOptions(options);
};

/**
  Refund an existing charge by its ID.
  Can also be used to release a pre-authorization that has not been captured.

  @method refundCharge
  @static
  @param {string} chargeId The charge ID.
  @return {Promise} Resolves if the refund is successful.
*/
exports.refundCharge = function (chargeId) {
  const options = {
    method: "POST",
    url: API_URL + "/kiosk/stripe/refund",
    headers: {
      "Authorization": AUTHORIZATION_KEY
    },
    body: {
      "chargeId": chargeId
    },
    json: true
  };

  return createPromiseForRequestOptions(options);
};

function createPromiseForRequestOptions (options) {
  return new RSVP.Promise(function (resolve, reject) {
    request(options, function (error, response, body) {
      if (error) {
        // If there is an error, reject with it
        winston.log("warn", "APIClient request error", {
          "error": util.inspect(error),
          "endpoint": options.url
        });
        reject(error);
      }
      else if (response.statusCode === 200) { // Check for the correct status code
        resolve(body);
      }
      else {
        winston.log("warn", "APIClient request failed", {
          "endpoint": options.url,
          "statusCode": response.statusCode,
          "responseBody": body
        });
        const error = new Error(body.message);
        error.code = body.code;
        error.type = body.type;
        error.rawType = body.rawType;
        error.statusCode = response.statusCode;
        reject(error);
      }
    });
  });
}
