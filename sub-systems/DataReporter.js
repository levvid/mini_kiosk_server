/*this is used to post the order data to MongoDB*/

var mongodb = require("mongodb");
var jsonfile = require("jsonfile");
var winston = require("winston");

const MONGO_PASSWORD = "Dr0pW%40t3r"

//uses %40 to handle @ sign error in password below
var dbHost = "mongodb+srv://dropwater:"+MONGO_PASSWORD+"@drop-water-bfrql.mongodb.net/<dbname>?retryWrites=true&w=majority";
var dbObject;
var MongoClient = mongodb.MongoClient;

/**
this method pulls the data from the MongoDB server online
@method postOrderData
**/
module.exports.postOrderData = function(drinkPrice, orderObject, refillVolume, paid){
  //paid is a bool letting us know whether or not a user used a free drink code
  //Connecting to the Mongodb instance.
  //Make sure your mongodb daemon mongod is running on port 27017 on localhost
  //gets the date, kiosk, price, and additive data and formats them for the data transfer
  var dateTime = getDateTime();
  var kioskID = getKioskID();
  var data = orderDataFormatter(kioskID, dateTime, drinkPrice, orderObject, refillVolume, paid);
  MongoClient.connect(dbHost, function(err, db){
    if ( err ){
      winston.log("debug", "cannot store data",err);
      return;
    }
    dbObject = db;
    var database = dbObject.db("DropWaterOrderData");
    //finds all of the data from the purchase collection on MongoDB
    database.collection("P3-Orders").insertOne(data).then(function(){
      db.close();
    });
  });
  return;
};

/**
this method posts the error data to the MongoDB server online
@method postErrorData
**/
module.exports.postErrorData = function(errorData){
  //Connecting to the Mongodb instance.
  //Make sure your mongodb daemon mongod is running on port 27017 on localhost
  //gets the date, kiosk, price, and additive data and formats them for the data transfer
  var dateTime = getDateTime();
  var kioskID = getKioskID();
  var data = errorDataFormatter(kioskID, dateTime, errorData);
  MongoClient.connect(dbHost, function(err, db){
    if ( err ){
      winston.log("debug", "no internet cannot store error data",err);
      return;
    }
    dbObject = db;
    var database = dbObject.db("dropwater");
    //finds all of the data from the purchase collection on MongoDB
    database.collection("P1-ErrorData").insert(data).then(function(){
      db.close();
    });
  });
  return;
};

/**
this method posts the error data to the MongoDB server online
@method postErrorData
**/
module.exports.postRefillData = function(refillData){
  //Connecting to the Mongodb instance.
  //Make sure your mongodb daemon mongod is running on port 27017 on localhost
  //gets the date, kiosk, price, and additive data and formats them for the data transfer
  var dateTime = getDateTime();
  var kioskID = getKioskID();
  var data = refillDataFormatter(kioskID, dateTime, refillData);
  MongoClient.connect(dbHost, function(err, db){
    if ( err ){
      winston.log("debug", "no internet cannot store error data",err);
      return;
    }
    dbObject = db;
    var database = dbObject.db("dropwater");
    //finds all of the data from the purchase collection on MongoDB
    database.collection("RefillData").insert(data).then(function(){
      db.close();
    });
  });
  return;
};



/**
this method returns the datetime to be added to the mongoDB message
@method getDateTime
@returns dateTime
**/
function getDateTime() {
  var date = new Date();


  var hour = date.getHours();
  hour = (hour < 10 ? "0" : "") + hour;


  var min  = date.getMinutes();
  min = (min < 10 ? "0" : "") + min;

  var sec  = date.getSeconds();
  sec = (sec < 10 ? "0" : "") + sec;

  var year = date.getFullYear();

  var month = date.getMonth() + 1;
  month = (month < 10 ? "0" : "") + month;

  var day  = date.getDate();
  day = (day < 10 ? "0" : "") + day;

  return  [
    {"year" : year},
    {"month": month},
    {"day" : day},
    {"hour" : hour},
    {"min" : min},
    {"sec" : sec}
  ];

}


/**
this method returns the Kiosk ID to be added to the mongoDB message
@method getKioskID
@returns KioskID
**/
function getKioskID() {
  var kioskID = jsonfile.readFileSync("losant-config.json");
  kioskID = kioskID.LOSANT_DEVICE_ID;

  return kioskID;
}


/**
this method returns the drink Price to be added to the mongoDB message
@method orderDataFormatter
@returns formattedData
**/
function orderDataFormatter(kioskID, dateTime, drinkPrice, orderObject, refillVolume, paid) {
  var dataObject = {
    "data":[
      {
        "dateTime": dateTime
      },
      {
        "kioskID": kioskID
      },
      {
        "drinkPrice": drinkPrice
      },
      {
        "flavor": orderObject.flavor
      },
      {
        "caffeineLevel": orderObject.caffeine
      },
      {
        "dropBottle": orderObject.dropBottle
      },
      {
        "refillAmount": refillVolume
      },
      {
        "promo": paid
      }
    ]
  };
  return dataObject;
}

/**
this method returns the drink Price to be added to the mongoDB message
@method errorDataFormatter
@returns formattedData
**/
function errorDataFormatter(kioskID, dateTime, errorData) {
  var dataObject = {
    "data":[
      {
        "dateTime": dateTime
      },
      {
        "kioskID": kioskID
      },
      {
        "errorData": errorData
      }
    ]
  };
  return dataObject;
}
