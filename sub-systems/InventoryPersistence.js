var jsonfile = require("jsonfile");
var postal = require("postal");
var winston = require("winston");


module.exports.bottleRetrieved = function(){
  //decrement bottles inventory file
  var fileData = jsonfile.readFileSync("inventory.json");
  var towerString = fileData.towerSelected;
  var remaining;
  switch(towerString){
    case "L":
      remaining = fileData.bottles.leftTowerRemaining;
      remaining = remaining - 1;
      fileData.bottles.leftTowerRemaining = remaining;
      break;
    case "R":
      remaining = fileData.bottles.rightTowerRemaining;
      remaining = remaining - 1;
      fileData.bottles.rightTowerRemaining = remaining;
      break;
    default:
      return;
  }
  jsonfile.writeFileSync("inventory.json", fileData, {spaces: 2});
};

module.exports.getData = function(){
  //returns the additive data
  var fileData = jsonfile.readFileSync("inventory.json");
  return fileData;
};

module.exports.additivesDispensed = function(additiveDispensed, amountDispensed){
  //decrement additive in inventory file
  var fileData = jsonfile.readFileSync("inventory.json");
  var inventoryAdditives = fileData.additives;
  var amountRemaining;
  var count = 0;
  inventoryAdditives.forEach(function(additive){
    if(additive.name == additiveDispensed){
      amountRemaining = additive.remaining;
      amountRemaining = amountRemaining - amountDispensed;
      additive.remaining = amountRemaining;
      fileData.additives[count] = additive;
    }
    count = count + 1;
  });
  jsonfile.writeFileSync("inventory.json", fileData, {spaces: 2});
};

module.exports.refillAdditivesDispensed = function(orderData, ouncesDispensed){
  //decrement additive in inventory file
  winston.log("debug", "refillAdditivesDispensed order data: ", orderData);
  if(orderData.flavor == "water" && orderData.caffeine == 0){
    return;
  }
  var mLDispensed = ouncesDispensed * 29.57; //Conversion from ounces to Ml of total water dispensed
  var dispenseFile = jsonfile.readFileSync("additiveLookup.json");
  var dispenseData = dispenseFile.additives;
  var inventoryData = jsonfile.readFileSync("inventory.json");
  var inventoryAdditives = inventoryData.additives;
  var amountRemaining;
  var count = 0;
  inventoryAdditives.forEach(function(additive){
    var amountDispensed = 0;
    if(additive.name == orderData.flavor){
      //found the ordered additive in the inventory file
      amountRemaining = additive.remaining;

      dispenseData.forEach(function(dispenseKey){
        //Look up the concentration of the additive dispensed in mL
        if(dispenseKey.name == additive.name){
          amountDispensed = dispenseKey.concentration * (mLDispensed/400); //mLDispensed / 400 is the amount dispensed in a normal bottle and thats what the concentration is normalized to.
        }
      })
      amountRemaining = amountRemaining - amountDispensed;
      additive.remaining = parseFloat(amountRemaining.toFixed(2));
      inventoryData.additives[count] = additive;
    }
    if(additive.name == "caffeine"){
      amountRemaining = additive.remaining;
      switch(orderData.caffeine){
        case 1:
          //20mg of Caffeine is 1.68mL
          amountDispensed = 1.68 * (mLDispensed/400); //mLDispensed / 400 is the amount dispensed in a normal bottle and thats what the concentration is normalized to.
          break;
        case 2:
          //40mg of caffeine is 3.37mL
          amountDispensed = 3.37 * (mLDispensed/400);
          break;
        case 3:
          //70mg of caffeine is 5.9mL
          amountDispensed = 5.9 * (mLDispensed/400);
          break;
        default:
          amountDispensed = 0;
      }
      amountRemaining = amountRemaining - amountDispensed;
      additive.remaining = parseFloat(amountRemaining.toFixed(2));
      inventoryData.additives[count] = additive;
    }
    count = count + 1;
  });
  jsonfile.writeFileSync("inventory.json", inventoryData, {spaces: 2});
};
