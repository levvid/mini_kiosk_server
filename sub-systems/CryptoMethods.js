var Crypto = require("crypto");
var FS = require("fs");

var PUBLIC_KEY = FS.readFileSync("dsa-public-key.pem");

// `data` param should be a Buffer
// `signature` param must be a Buffer
exports.verifyMessage = function (data, signature) {
  var verifyObj = Crypto.createVerify("DSA");
  verifyObj.update(data);
  return verifyObj.verify(PUBLIC_KEY, signature);
};
