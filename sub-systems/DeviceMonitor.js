var Udev = require("udev");
const winston = require("winston");
const pm2 = require("pm2");

const DEVNAME = "DEVNAME";
const VENDOR_ID_KEY = "ID_VENDOR_ID";

var removedDevice;
/**
  DeviceMonitor is reponsible for detecting serial port devices as they are
  plugged in. The path for new devices will be sent to the callback function.

  @class DeviceMonitor
  @constructor
  @param {Function} addCallback Called when a serial port device is detected.
  @param {String} addCallback.path The path for the new device.
  @param {Function} removeCallback Called when a serial port device is removed.
  @param {String} removeCallback.path The path for the removed device.
*/
function DeviceMonitor (addCallback, removeCallback) {
  /**
    The callback to be invoked when a new serial port is added. Passed in via
    the constructor.
    @property addCallback
    @type Function
  */
  this.addCallback = addCallback;
  /**
    The callback to be invoked when a serial port is removed. Passed in via the
    constructor.
    @property removeCallback
    @type Function
  */
  this.removeCallback = removeCallback;
  this.monitor = Udev.monitor();

  /**
    An object containing known serial port devices. Key is the path, and value
    is the device object.
    @property matchingDevices
    @type Object
  */
  this.matchingDevices = {};

  // Process initially connected devices
  Udev.list().forEach(function (device) {
    this.processNewDevice(device);
  }, this);

  // After processing already connected devices, always listen for `add` event
  this.startMonitoringAdd();

  // We always want to know if a serial device is disconnected
  //this.startMonitoringRemove();
}

/**
  Determine if a `udev` device is a serial port.

  @private
  @method testForSerial
  @param {Object} device The `udev` device to test.
  @return {Boolean} True if device is serial port.
*/
function testForSerial (device) {
  // Check device SUBSYSTEM is `tty`
  var subsystem = "SUBSYSTEM";
  if (!(device.hasOwnProperty(subsystem) &&
    device[subsystem] === "tty")) { return false; }

  // Check device DEVNAME starts with `/dev/tty`
  if (!(device.hasOwnProperty(DEVNAME) &&
    device[DEVNAME].indexOf("/dev/tty") === 0)) { return false; }

  if (
    !(
      device.hasOwnProperty(VENDOR_ID_KEY) &&
      DeviceMonitor.validVendorId(device[VENDOR_ID_KEY])
    )
  ) { return false; }

  // All checks have passed
  return true;
}

/**
 * Check vendor ID against valid known vendors.
 *
 * @method validVendorId
 * @static
 * @param {string} vendorId
 * @return {boolean} Return true iff the vendor ID is valid.
 */
DeviceMonitor.validVendorId = function (vendorId) {
  const validIds = [
    "2341", // Arduino
    "1b4f", // SparkFun
    "0403", // chipKIT
  ];
  if (validIds.indexOf(vendorId) >= 0) {
    // vendorId was found
    return true;
  } else {
    return false;
  }
};

/**
  Begin monitoring `udev` library for device connections. If a new device is
  identified as a serial port, it is added to `matchingDevices`.

  @method startMonitoringAdd
*/
DeviceMonitor.prototype.startMonitoringAdd = function() {
  winston.log("verbose", "Starting monitoring for new devices.");
  // Setup monitoring of add events
  var self = this;
  this.monitor.on("add", function (device) {
    self.processNewDevice(device);
  });
};

/**
  Listen for device removal events from `udev` library. The device will be
  removed from `matchingDevices` and the `removeCallback` function will be
  invoked.

  @method startMonitoringRemove
*/
DeviceMonitor.prototype.startMonitoringRemove = function() {
  var self = this;
  this.monitor.on("remove", function (device) {
    if (testForSerial(device)) {
      var path = device[DEVNAME];
      winston.log("info", "A serial port device was disconnected", {path: path});
      if (self.matchingDevices.hasOwnProperty(path)) {
        // Remove from matched devices
        delete self.matchingDevices[path];
        // Invoke removeCallback with device's DEVNAME
        if (typeof(self.removeCallback) === "function") {
          self.removeCallback(path);
        }
      } else {
        winston.log("warn", "Removed device was not previously matched", {path: path});
      }
    }
  });
};

/**
  All found devices should be passed to this method. First passes device through
  `testForSerial` function.
  Responsible for invoking the callback function and passing the device path.
  Also updates `matchingDevices` to contain known devices.

  @method processNewDevice
*/
DeviceMonitor.prototype.processNewDevice = function (device) {
  if (!testForSerial(device)) { return; } // Device did not pass test

  var path = device[DEVNAME];

  // Check if it already exists in the array
  if (this.matchingDevices.hasOwnProperty(path)) {
    winston.log("warn", "Device detected multiple times", {path: path});
    pm2.restart("all");

  } else {
    winston.log("verbose", "DeviceMonitor Matching device found", {path: path});
    // Invoke addCallback with device's DEVNAME
    if (typeof(this.addCallback) === "function") {
      this.addCallback(path);
    }
    // Add to known matched devices
    this.matchingDevices[path] = device;
  }
};

module.exports = DeviceMonitor;
