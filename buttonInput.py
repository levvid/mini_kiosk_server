import RPi.GPIO as GPIO
import time
import sys

right_button = 19
center_button = 23
left_button = 21

def main():
    prev_value_right = None
    prev_value_center = None
    prev_value_left = None
    # Pin Setup:
    GPIO.setmode(GPIO.BOARD)  # BCM pin-numbering scheme from Raspberry Pi
    GPIO.setup(right_button, GPIO.IN)  # set pin as an input pin
    GPIO.setup(center_button, GPIO.IN)
    GPIO.setup(left_button, GPIO.IN)
    try:
        while True:
            value = GPIO.input(right_button)
            if value != prev_value_right:
                if value == GPIO.LOW:
                    print("right")
                    #sys.stdout.write("right")
            prev_value_right = value

            value = GPIO.input(center_button)
            if value != prev_value_center:
                if value == GPIO.LOW:
                    print("center")
                    #sys.stdout.write("center")
            prev_value_center = value

            value = GPIO.input(left_button)
            if value != prev_value_left:
                if value == GPIO.LOW:
                    print("left")
                    #sys.stdout.write("center")
            prev_value_left = value

            time.sleep(0.25)
    finally:
        GPIO.cleanup()

if __name__ == '__main__':
    main()
