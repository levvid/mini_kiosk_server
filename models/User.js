const mongoose = require("mongoose");
const Schema = mongoose.Schema;
// Create Schema
const UserSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  date_created: {
    type: Date,
    default: Date.now,
  },
  uuid: {
    type: String,
    required: true,
  },
  is_admin: {
    type: Boolean,
    default: false
  }
});
module.exports = User = mongoose.model("users", UserSchema);
