module.exports = {
  apps: [
    {
      name: "Drop-Kiosk-UI-Server",
      script: "Drop-Kiosk-UI-Server.js",
      autorestart: false,
      env: {
        "NODE_ENV": "production"
      }
    },
    {
      name: "log-service",
      script: "log-service.js",
      autorestart: true,
      env: {
        "NODE_ENV": "production"
      }
    }
  ]
};
