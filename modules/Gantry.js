"use strict";
var jsonfile = require("jsonfile");
var util = require("util");
var BaseModule = require("./BaseModule");
var RSVP = require("rsvp");
const postal = require("postal");
var winston = require("winston");

const MODULE_NAME = "Gantry";
const postalChannel = postal.channel(MODULE_NAME.toLowerCase());

/**
  @class Gantry
  @extends BaseModule
  @constructor
  @param {SerialPort} serialPort The open serial port.
*/
function Gantry (serialPort) {
  this.serialPort = serialPort;
  this.tower = null;

  this.moduleName = MODULE_NAME;
}
util.inherits(Gantry, BaseModule);

Gantry.POSITION = {
  3: "A1",  2: "A2", 1:"A3", // All rows grab from right to left
  6: "B1",  5: "B2", 4:"B3",// All rows grab from right to left
  9: "C1",  8: "C2", 7:"C3",  // All rows grab from right to left
  12: "D1",  11: "D2", 10:"D3", // All rows grab from right to left
};

// Gantry error codes
Gantry.ERROR = {
  towerEmpty: "towerEmpty", // Tower out of bottles
  missAttemptExceeded: "missAttemptExceeded", // Gantry bottle retrieve missed too many times, exceeded bottle miss attempts
  noBottleAttemptExceeded: "noBottleAttemptExceeded"
};
// Allow access from gantry instance as well
Gantry.prototype.ERROR = Gantry.ERROR;

/**
  Asks the tower for the next bottle position.
  Performs one bottle grab attempt at that position.
  Possible statuses after the attempt are: retrieved, missed, and not there.

  @method retrievee
  @return {Promise}
*/
Gantry.prototype.retrieveNextBottle = function() {
  var self = this;
  return this.tower.nextBottlePosition().then(function(nextPosition){
    if (nextPosition) {
      return self.moveToPosition(nextPosition);
    } else {
      return RSVP.reject(Gantry.ERROR.towerEmpty);
    }
  });
};

/**
  Handles all three of the possible responses from #retrieveNextBottle().
  Encapsulates into a promise that resolves when the bottle has either been
  retrieved or skipped.

  @method retrieveNextBottleSuccess
  @return {Promise}
*/
Gantry.prototype.retrieveNextBottleSuccess = function() {
  var self = this;
  function promiseCallback (resolve, reject, missAttempts, noBottle) {
    var missAttemptsCount = missAttempts || 1; // When called without thrid param and in initial case, missAttempts param will be `null`
    var noBottleCount = noBottle || 1;
    self.retrieveNextBottle().then(function (code) {
      if (code === "K") {
        self.tower.bottleRetrieved();
        self.doubleBottleCheck().then(function (code) {
          if (code === "S"){
            //Single bottle Continue on process
            self.tower.gantryClearedTower(); // Allows tower to raise level if necessary
            resolve();
          }
          else if (code === "D" || code === "F" || code === "G") {
            //Double Bottle or bad flap seal Throw Bottle away
            postalChannel.publish("multiple-bottle-grab"); //Move to else if code === D in the function above
            self.throwAwayBottle().then(function() {
              promiseCallback(resolve, reject, 1, 1); // Continue on to next bottle
            });
          }
        });
      } else if (code === "N") {
        postalChannel.publish("no-bottle-found");
        self.tower.bottleSkipped();
        self.tower.gantryClearedTower(); // Allows tower to raise level if necessary
        if(noBottleCount == 3){
          reject(Gantry.ERROR.noBottleAttemptExceeded);
        }else{
          //dont call the callback if the nobottle attempt is exceeded
          promiseCallback(resolve, reject, 1, noBottleCount + 1);
        }
      } else if (code === "M" || code === "R") {
        //Bottle collision or missed bottle
        postalChannel.publish("bottle-missed");
        if (missAttemptsCount === 5) {
          reject(Gantry.ERROR.missAttemptExceeded);
        } else {
          // Try to retrieve again and increment miss attempts
          promiseCallback(resolve, reject, missAttemptsCount + 1, 1);
        }
      } else if (code === "G") {
        //Bottle collision likely, have the machine garbage bottles
        self.throwAwayBottle().then(function() {
          promiseCallback(resolve, reject, 1, 1); // Continue on to next bottle
        });
      }
    }, reject);
  }
  return new RSVP.Promise(promiseCallback);
};

Gantry.prototype.doubleBottleCheck = function() {
  this.write("BC");
  this.createTimeoutForEvent("BC");
  return this.checkStatus("SD");
};

Gantry.prototype.probeBottle = function() {
  this.write("PP");
  this.createTimeoutForEvent("PP");
  return this.checkStatus("K");
};

Gantry.prototype.placeProbeInUV = function() {
  this.write("FP");
  return this.checkStatus("K");
};

Gantry.prototype.deliverBottle = function() {
  this.write("00");
  this.createTimeoutForEvent("00");
  return this.checkStatus("K");
};

Gantry.prototype.checkIfHoldingBottle = function() {
  this.write("BB");
  this.createTimeoutForEvent("BB");
  return this.checkStatus("KN");
};

Gantry.prototype.moveToPosition = function (position) {
  var code = Gantry.POSITION[position];
  this.write(code);
  this.createTimeoutForEvent(code);
  return this.checkStatus("KMNRG");
};

Gantry.prototype.closeBottle = function() {
  this.write("FC");
  this.createTimeoutForEvent("FC");
  const promise = this.checkStatus("KGF");
  promise.then(responseCode => {
    if (responseCode === "G"|| responseCode === "F") {
      postalChannel.publish("flap-close-failure");
    }
  }, () => {
    postalChannel.publish("fatal-error.flap-close");
  });
  return promise;
};

Gantry.prototype.throwAwayBottle = function() {
  postalChannel.publish("throw-away-bottle");
  this.write("GG");
  //this.createTimeoutForEvent("GG");
  return this.checkStatus("K");
};

Gantry.prototype.switchTowerSelected = function() {
  var fileData = jsonfile.readFileSync("inventory.json");
  var currentTower = fileData.towerSelected;
  if(currentTower == "R"){
    fileData.towerSelected = "L";
  }else{
    //defaults to Right tower
    fileData.towerSelected = "R";
  }
  jsonfile.writeFileSync("inventory.json", fileData, {spaces: 2});
  this.write(fileData.towerSelected);
  return this.checkStatus("K");
};

Gantry.prototype.sendCurrentTower = function(){
  var fileData = jsonfile.readFileSync("inventory.json");
  var currentTower = fileData.towerSelected;
  this.write(currentTower);
  return this.checkStatus("K");
};

Gantry.prototype.sendNewTowerSelection = function(newTower){
  var fileData = jsonfile.readFileSync("inventory.json");
  fileData.towerSelected = newTower;
  jsonfile.writeFileSync("inventory.json", fileData, {spaces: 2});
  this.write(newTower);
  return this.checkStatus("K");
};

module.exports = Gantry;
