var util = require("util");
var BaseModule = require("./BaseModule");
var Gantry = require("./Gantry");
var postal = require("postal");
var jsonfile = require("jsonfile");
var winston = require ("winston");

const MODULE_NAME = "Tower";
var postalChannel = postal.channel(MODULE_NAME.toLowerCase());

/**
  @class Tower
  @extends BaseModule
  @constructor
  @param {SerialPort} serialPort The open serial port.
  @param {Number} bottlesRemaining The number of bottles in the tower.
*/
function Tower (serialPort) {
  this.serialPort = serialPort;
  var inventoryData = jsonfile.readFileSync("inventory.json");
  this.towerSelected = inventoryData.towerSelected;
  if(inventoryData.towerSelected == "R"){
    this.bottlesRemaining = inventoryData.bottles.rightTowerRemaining;
  }else{
    //left tower is selected
    this.bottlesRemaining = inventoryData.bottles.leftTowerRemaining;
  }
  this.moduleName = MODULE_NAME;
  this._needsToRaiseLevel = false;

  this.gantry = null;
}
util.inherits(Tower, BaseModule);

Tower.numberOfPositions = Object.keys(Gantry.POSITION).length;

Tower.prototype.nextBottlePosition = function() {
  var self = this;
  return new Promise(function(resolve, reject){
    var inventoryData = jsonfile.readFileSync("inventory.json");
    var currentTower = inventoryData.towerSelected;
    if(currentTower == "R"){
      self.bottlesRemaining = inventoryData.bottles.rightTowerRemaining;
    }else{
      //Current tower is left tower
      self.bottlesRemaining = inventoryData.bottles.leftTowerRemaining;
    }
    if (self.bottlesRemaining <= 0) {
      //if it's 0 switch towers and continue
      //unless both towers are out of bottles then youre done.
      if((currentTower == "R" && inventoryData.bottles.leftTowerRemaining > 0) || (currentTower == "L" && inventoryData.bottles.rightTowerRemaining > 0)){
        //Switch to the other tower
        self.gantry.switchTowerSelected().then(function(){
          if(currentTower == "R"){
            currentTower = "L";
            self.towerSelected = "L";
            self.bottlesRemaining = inventoryData.bottles.leftTowerRemaining;
          }else{
            currentTower = "R";
            self.towerSelected = "R";
            self.bottlesRemaining = inventoryData.bottles.rightTowerRemaining;
          }
          var i = Tower.numberOfPositions - ( ( self.bottlesRemaining - 1 ) % Tower.numberOfPositions );
          resolve(i);
        });
      }else{
        resolve(null) ; // No bottles left
      }
    }else{
      var i = Tower.numberOfPositions - ( ( self.bottlesRemaining - 1 ) % Tower.numberOfPositions );
      resolve(i);
    }
  });
};


Tower.prototype.bottleRetrieved = function() {
  this._decrementBottlesRemaining();
};

Tower.prototype.bottleSkipped = function() {
  this._decrementBottlesRemaining();
};

Tower.prototype._needToRaiseLevel = function() {
  if (this.bottlesRemaining <= 0) {
    return false;
  }
  return this.bottlesRemaining % Tower.numberOfPositions === 0;

};

/**
  Subtract one from the number of bottles remaining. If the bottle just
  retrieved was the last on its level, raise the tower level by one. This
  method sends Postal messages on the "tower" channel, with the topic
  "bottleRetrieved". The message data contains:
  * `bottlesRemaining`: The number of bottles still available in the tower
  @method _decrementBottlesRemaining
  @private
*/
Tower.prototype._decrementBottlesRemaining = function() {
  this.bottlesRemaining--;
  winston.log("debug", "_decrementBottlesRemaining hit");
  var fileData = jsonfile.readFileSync("inventory.json");
  var towerString = fileData.towerSelected;
  if(this.towerSelected == "R"){
    fileData.bottles.rightTowerRemaining = this.bottlesRemaining;
  }else{
    fileData.bottles.leftTowerRemaining = this.bottlesRemaining;
  }
  jsonfile.writeFileSync("inventory.json", fileData, {spaces: 2});

  if (this._needToRaiseLevel()) {
    this._needsToRaiseLevel = true;
  }
  this.log("info", "decrement bottles remaining", {
    "bottlesRemaining": this.bottlesRemaining
  });
};

Tower.prototype.resetLeftTower = function() {
  this.write("R");
  return this.checkStatus("K");
};

Tower.prototype.resetRightTower = function() {
  this.write("W");
  return this.checkStatus("K");
};


Tower.prototype.raiseLeftTower = function() {
  //Raise Left tower all the way
  this.write("F");
  return this.checkStatus("K");
};

Tower.prototype.raiseRightTower = function(){
  //raise right tower all the way
  this.write("G");
  return this.checkStatus("K");
};

Tower.prototype.raiseLevel = function() {
  //Raise one level
  //Marc: check which tower we're working with first
  if(this.towerSelected == "R"){
    this.write("Y");
  }else{
    //left tower Selected
    this.write("U");
  }
  return this.checkStatus("K");
};

// check if needsRaise is true, if true then send command
Tower.prototype.gantryClearedTower = function() {
  if (this._needsToRaiseLevel === true) {
    this.raiseLevel();
    this._needsToRaiseLevel = false;
  }
};

Tower.prototype._currentLevel = function() {
  var levelFraction = this.bottlesRemaining / Tower.numberOfPositions;
  return Math.floor(levelFraction);
};

Tower.prototype.getTankLevel = function(){
  var self = this;
  return new Promise(function(resolve, reject){
    self.write("T");
    self.createTimeoutForEvent("T");
    resolve(self.checkStatusData("K"));
  });
};

Tower.prototype.resetTankLevel = function(){
  this.write("Y");
  return this.checkStatus("K");

};

module.exports = Tower;
