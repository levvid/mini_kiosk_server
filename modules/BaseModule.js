var RSVP = require("rsvp");
var util = require("util");
var winston = require("winston");

/**
  This is an abstract superclass for the modules that communicate over a serial port.
  It implements `#checkStatus()` to watch serial data and return a promise.
  @class BaseModule
  @constructor
*/
function BaseModule (serialPort) {
  this.serialPort = serialPort;
  this.moduleName = "BaseModule";
  this.waitingForStatus = false; // Track if there is a command that has not received a response
  this.originalCommand = null; // Track the command data that is waiting for a response
  this.timeoutExists = false;
  this.eventTimeout;
}

/**
  This method observes data coming in from the serial port and resolves or
  rejects the returned promise based on the data.
  Only uppercase characters are considered responses. Lowercase characters can
  be used by the module for logging or debugging purposes.

  The `acceptedValues` param is a string of uppercase characters that are
  considered valid responses from the serial port. For example, most modules
  return 'K' if the command was successful.

  The promise will reject if there is an uppercase character from the port that
  is not one of the accepted characters.
  It will also reject if there are multiple uppercase characters from the port,
  as only one response is expected per command.

  @method checkStatus
  @param {String} acceptedValues String of characters that are accepted responses.
  @return {Promise} Resolves with accepted character.
*/
BaseModule.prototype.checkStatus = function (acceptedValues) {
  const originalCommand = this.originalCommand; // Include the original command for each of the response cases
  var self = this;
  var port = this.serialPort;
  return new RSVP.Promise(function (resolve, reject) {
    port.on("data", function dataCallback (data) {
      var string = data.toString();
      // Array of characters in the response data that match the acceptedValues parameter
      var matchArray = string.match(new RegExp(util.format("[%s]", acceptedValues), "g"));
      // All uppercase characters in the response data
      var allUppercase = string.match(/[A-Z]/g);
      if (matchArray && matchArray.length === 1 && allUppercase.length === 1) {
        port.removeListener("data", dataCallback);
        self.log("debug", "BaseModule response", {"response": matchArray[0], "data": string, "originalCommand": originalCommand}, true);
        resolve(matchArray[0]);
      } else if (matchArray && matchArray.length > 1) {
        port.removeListener("data", dataCallback);
        self.log("error", "BaseModule multiple responses error", {"data": string, "originalCommand": originalCommand}, true);
        reject(matchArray);
      } else if (allUppercase) {
        // No matched chars, check if there were any uppercase letters that didn't match
        port.removeListener("data", dataCallback);
        self.log("error", "BaseModule unmatched uppercase response characters", {"data": string, "originalCommand": originalCommand}, true);
        reject(allUppercase);
      } else {
        self.log("debug", "BaseModule unhandled response case", {"data": string, "originalCommand": originalCommand}, true);
      }
    });
  }).then(function (value) {
    // A response has been received
    self.waitingForStatus = false;
    self.originalCommand = null;
    if(self.timeoutExists == true){
      self.clearTimeoutForEvent();
    }
    return value;
  }, "BaseModule checkStatus"); // RSVP Promise label for debugging
};


/*
@method CheckData
@param {String} acceptedValues String of characters that are accepted responses.
@return {Promise} Resolves with accepted character.
*/
BaseModule.prototype.checkStatusData = function (acceptedValues) {
  const originalCommand = this.originalCommand; // Include the original command for each of the response cases
  var self = this;
  var port = this.serialPort;
  return new RSVP.Promise(function (resolve, reject) {
    port.on("data", function dataCallback (data) {
      var string = data.toString();
      // Array of characters in the response data that match the acceptedValues parameter
      var matchArray = string.match(new RegExp(util.format("[%s]", acceptedValues), "g"));
      // All uppercase characters in the response data
      var allUppercase = string.match(/[A-Z]/g);
      if (matchArray && matchArray.length === 1 && allUppercase.length === 1) {
        port.removeListener("data", dataCallback);
        self.log("debug", "BaseModule response", {"response": matchArray[0], "data": string, "originalCommand": originalCommand}, true);
        resolve(string);
      } else if (matchArray && matchArray.length > 1) {
        port.removeListener("data", dataCallback);
        self.log("error", "BaseModule multiple responses error", {"data": string, "originalCommand": originalCommand}, true);
        reject(matchArray);
      } else if (allUppercase) {
        // No matched chars, check if there were any uppercase letters that didn't match
        port.removeListener("data", dataCallback);
        self.log("error", "BaseModule unmatched uppercase response characters", {"data": string, "originalCommand": originalCommand}, true);
        reject(allUppercase);
      } else {
        self.log("debug", "BaseModule unhandled response case", {"data": string, "originalCommand": originalCommand}, true);
      }
    });
  }).then(function (value) {
    // A response has been received
    self.waitingForStatus = false;
    self.originalCommand = null;
    if(self.timeoutExists == true){
      self.clearTimeoutForEvent();
    }
    return value;
  }, "BaseModule checkStatus"); // RSVP Promise label for debugging
};

/**
  This helper method allows a common interface for modules to write data to the
  module's serial port. It is responsible for logging the command and forwarding
  it to the serialPort.

  @method write
  @param {String} command Command to be sent to the serial port.
*/
BaseModule.prototype.write = function (data) {
  if (this.waitingForStatus) {
    // Don't allow for next command to write if there is a command that has not yet received a response
    this.log("error", "BaseModule existing command without response error", {"unsentCommand": data}, true);
    return;
  }
  this.log("debug", "BaseModule command", {"command": data}, true);
  this.serialPort.write(data, function (error) {
    if (error) {
      winston.log("error", "BaseModule write error", {error: util.inspect(error)});
    }
  });
  this.waitingForStatus = true;
  this.originalCommand = data;
};


/**
  This helper method allows a common interface for modules to create a timeout
  for certain movements or commands to ensure that the module
  has received and executed the command

  @method createTimeoutForEvent
  @param {String} command Command that was sent to the serial port.
*/
BaseModule.prototype.createTimeoutForEvent = function (data) {
  this.timeoutExists = true;
  var self = this;
  this.eventTimeout = setTimeout(function() {
    winston.log("debug", "module response timeout hit", data);
    self.originalCommand = null;
    self.waitingForStatus = false;
    self.write(data);
  }, 13000);
};

//Marc: Did this to silence logs on Lighting Module checking if the water tank is turned on.

BaseModule.prototype.checkStatusWithoutLogs = function (acceptedValues) {
  const originalCommand = this.originalCommand; // Include the original command for each of the response cases
  var self = this;
  var port = this.serialPort;
  return new RSVP.Promise(function (resolve, reject) {
    port.on("data", function dataCallback (data) {
      var string = data.toString();
      // Array of characters in the response data that match the acceptedValues parameter
      var matchArray = string.match(new RegExp(util.format("[%s]", acceptedValues), "g"));
      // All uppercase characters in the response data
      var allUppercase = string.match(/[A-Z]/g);
      if (matchArray && matchArray.length === 1 && allUppercase.length === 1) {
        port.removeListener("data", dataCallback);
        //self.log("debug", "BaseModule response", {"response": matchArray[0], "data": string, "originalCommand": originalCommand}, true);
        resolve(matchArray[0]);
      } else if (matchArray && matchArray.length > 1) {
        port.removeListener("data", dataCallback);
        self.log("error", "BaseModule multiple responses error", {"data": string, "originalCommand": originalCommand}, true);
        reject(matchArray);
      } else if (allUppercase) {
        // No matched chars, check if there were any uppercase letters that didn't match
        port.removeListener("data", dataCallback);
        self.log("error", "BaseModule unmatched uppercase response characters", {"data": string, "originalCommand": originalCommand}, true);
        reject(allUppercase);
      } else {
        self.log("debug", "BaseModule unhandled response case", {"data": string, "originalCommand": originalCommand}, true);
      }
    });
  }).then(function (value) {
    // A response has been received
    self.waitingForStatus = false;
    self.originalCommand = null;
    if(self.timeoutExists == true){
      self.clearTimeoutForEvent();
    }
    return value;
  }, "BaseModule checkStatus"); // RSVP Promise label for debugging
};

BaseModule.prototype.writeWithoutLogs = function (data) {
  if (this.waitingForStatus) {
    // Don't allow for next command to write if there is a command that has not yet received a response
    this.log("error", "BaseModule existing command without response error", {"unsentCommand": data}, true);
    return;
  }
  //this.log("debug", "BaseModule command", {"command": data}, true);
  this.serialPort.write(data, function (error) {
    if (error) {
      winston.log("error", "BaseModule write error", {error: util.inspect(error)});
    }
  });
  this.waitingForStatus = true;
  this.originalCommand = data;
};

/**
  This helper method allows a common interface for modules to clear the timeout
  for certain movements or commands after the module has received and responded
  to the command

  @method clearTimeoutForEvent
  @param None
*/
BaseModule.prototype.clearTimeoutForEvent = function () {
  this.timeoutExists = false;
  clearTimeout(this.eventTimeout);
};



/**
  Attach this module's name to the given log message and metadata and log the
  result to winston.

  @method log
  @param {string} level The level at which to log the message
  @param {string} message The log message
  @param {Object} meta Any associated log message data
  @param {Boolean} [exactMessage] Don't change message string
*/
BaseModule.prototype.log = function (level, message, meta, exactMessage) {
  meta = meta || {};
  meta.moduleName = this.moduleName;
  var finalMessage = exactMessage ? message : this.moduleName + " " + message;
  winston.log(level, finalMessage, meta);
};

module.exports = BaseModule;
