var RSVP = require("rsvp");
var util = require("util");
var BaseModule = require("./BaseModule");
var postal = require("postal");
var jsonfile = require("jsonfile");
var winston = require("winston");
const MODULE_NAME = "Additive";
var postalChannel = postal.channel(MODULE_NAME.toLowerCase());

/**
  @class Additive
  @extends BaseModule
  @constructor
  @param {SerialPort} serialPort
  @param {Array} additives This array should contain objects, each of which
  corresponds to one additive dispenser. Each object should contain `name`, the
  name of the additive, and `remaining`, the number of doses left in that
  additive's dispenser. The order and number of elements in the array correpond
  to the order and number of additive dispensers in the machine. If an additive
  dispenser should be ignored, set its corresponding array entry to `null`. This
  array determines the mapping from a drink order, which contains additive
  names, to an additive module code, which is based on dispenser positions
  within the additive hardware module.
*/

function RefillAdditive (serialPort, additives) {
  this.serialPort = serialPort;
  this.moduleName = MODULE_NAME;
  this.refillInProgress = false;
  /**
    An array containing a copy of each of the objects in the `additives`
    array passed to the constructor. The array entries are copied to avoid
    sharing state with the code invoking the constructor.
    @property additivesArray
    @type Array
  */
  var additivesArray = [];
  additives.forEach(function(additive) {
    var entry;
    if (additive === null) {
      entry = null;
    } else {
      entry = {
        name: additive.name,
        remaining: additive.remaining,
        concentration: additive.concentration
      };
    }
    additivesArray.push(entry);
  });
  this.additivesArray = additivesArray;
}
util.inherits(RefillAdditive, BaseModule);


function getmLPerDose(additiveName){
  var dispenseRateJson = jsonfile.readFileSync("additive-dispense-rate.json");
  winston.log("debug", "additiveName", additiveName);
  var count = 0;
  while(count < dispenseRateJson.length){
    if(dispenseRateJson[count].name == additiveName){
      return dispenseRateJson[count].mlPerNum;
    }
    count++;
  }
}

RefillAdditive.prototype.startDispense = function() {
  //begins dispensing water from refill side
  this.write("F");
  return this.checkStatus("K");
};

RefillAdditive.prototype.setLightColor = function(color) {
  //begins dispensing water from refill side
  this.write(color);
  return this.checkStatus("K");
};

RefillAdditive.prototype.sendRefillTemperature = function(temperature) {
  var code = "";
  if(temperature == "room"){
    //Letters seem backwards but blame Ron for that
    code = "C";
  }else{
    code = "W";
  }
  this.write(code);
  return this.checkStatus("K");
};

RefillAdditive.prototype.endDispense = function() {
  //stops dispensing water from refill side
  this.write("V");
  return this.checkStatus("K");
};

RefillAdditive.prototype.setDefaultRefill = function() {
  //Reverts back to water refill enabled only
  var self = this;
  return this.turnOffRefill().then(function(){
    return self.sendWaterDefault();
  });
};

RefillAdditive.prototype.turnOffRefill = function(){
  this.write("O");
  return this.checkStatus("K");
}

RefillAdditive.prototype.resetWaterToCold = function(){
  winston.log("debug", "Resetting water to cold");
  this.write("W");  // reset water to cold
  return this.checkStatus("K");
}


RefillAdditive.prototype.sendWaterDefault = function(){
  var self = this;
  this.write("00000000");
  return this.checkStatus("K").then(function(){
    return self.resetWaterToCold();
  });
}

RefillAdditive.prototype.enableCarbonated = function() {
  this.write("C");
  this.createTimeoutForEvent("C");
  return this.checkStatus("K");
};

RefillAdditive.prototype.enableStillWater = function() {
  this.refillInProgress = true;
  this.write("W");
  this.createTimeoutForEvent("W");
  return this.checkStatus("K");
};

RefillAdditive.prototype.checkRefillEnabled = function() {
  //Asks the refillAdditive module if the tank is full
    this.write("D");
    return this.checkStatus("FK");
    //if the refill is in progress
};

RefillAdditive.prototype.checkDispenseStatus = function() {
  //Used to query dispense status while the person is doing a refillOrder
  //Keep track of the amount of liquid dispensed
  var self = this;
  return new Promise(function(resolve, reject){
    self.write("S");
    self.createTimeoutForEvent("S");
    resolve(self.checkStatusData("K"));
  });

};

RefillAdditive.prototype.disableRefillButton = function() {
  //Used to disable the refill button when the person has finished their refillOrder
  //We dont actually disable the button anymore
  return new Promise(function(resolve, reject){
    resolve("K");
  });

  /*this.write("O");
  this.createTimeoutForEvent("O");
  this.refillInProgress = false;
  return this.checkStatus("K");*/
};

RefillAdditive.prototype.setPurge = function(){
  this.write("N");
  return this.checkStatus("K");
}

RefillAdditive.prototype.sendPurgeCode = function(code){
  this.write(code);
  return this.checkStatus("K");
}

/**
  This method is used for dispensing flavor out of the .
  @method sendRefillOrder
  @param flavorString (Name of the flavor to be disspensed)
*/
RefillAdditive.prototype.sendRefillOrder = function(orderObject){
  winston.log("debug", "sendRefillOrderHit");
  var self = this;
  return self._generateAdditiveCode(orderObject.flavor, orderObject.caffeine).then(function(orderString){
    self.write(orderString);
    return self.checkStatus("K");
  });

};

/**
  Generate an additive code corresponding to the order object. The additive code
  is a string in which each character corresponds to the amount to dispense from
  the pump in the position corresponding to that character's index in the string.
  @method _generateAdditiveCode
  @param {Order} order Generate the code for this order.
  @return {String} The generated additive code.
  @private
*/
RefillAdditive.prototype._generateAdditiveCode = function (flavor, caffeineValue) {
  winston.log("debug", "flavor", flavor);
  winston.log("debug", "caffeine", caffeineValue);

  return new Promise(function(resolve, reject){
    var code = "";
    var jsonInventory = jsonfile.readFileSync("inventory.json");
    var additiveArray = ["0", "0", "0", "0", "0", "0", "0", "0"];
    var additiveLookup = jsonfile.readFileSync("additiveLookup.json");
    var index = 0;
    jsonInventory.additives.forEach(function(inventoryObject){
      if(inventoryObject.name == flavor){
        var mlPerNum = getmLPerDose(flavor)
        additiveLookup.additives.forEach(function(additiveLookupObject){
          if(flavor == additiveLookupObject.name){
            var concentration = additiveLookupObject.concentration;
            var desiredPumpValue = Math.round(concentration/mlPerNum);
            desiredPumpValue = desiredPumpValue + 48;
            if(desiredPumpValue > 126){
              //Did this because the ASCII table ran out
              winston.log("warn", "Tried to command too high of an additive pump value");
              desiredPumpValue = 126;
            }
            if(String.fromCharCode(desiredPumpValue) == "?" || String.fromCharCode(desiredPumpValue) == "X"){
              desiredPumpValue = desiredPumpValue + 1;
            }
            char = String.fromCharCode(desiredPumpValue);
            additiveArray[index] = char;
          }
        });
      }
      if(inventoryObject.name == "caffeine"){
        var mlPerNum = getmLPerDose("caffeine");
        switch(caffeineValue){
          case 0:
            additiveArray[index] = String.fromCharCode(48);
            break;
          case 1:
            //20mg
            var pumpValue = Math.round(1.68 / mlPerNum);
            pumpValue = pumpValue + 48;
            if(pumpValue > 126){
              //cap the pump value because the ASCII table ran out
              winston.log("warn", "Tried to command too high of an additive pump value");
              pumpValue = 126;
            }
            additiveArray[index] = String.fromCharCode(pumpValue);
            break;
          case 2:
            //40mg
            var pumpValue = Math.round(3.37 / mlPerNum);
            pumpValue = pumpValue + 48;
            if(pumpValue > 126){
              //cap the pump value because the ASCII table ran out
              winston.log("warn", "Tried to command too high of an additive pump value");
              pumpValue = 126;
            }
            additiveArray[index] = String.fromCharCode(pumpValue);
            break;
          case 3:
            //70mg
            var pumpValue = Math.round(5.9 / mlPerNum);
            pumpValue = pumpValue + 48;
            if(pumpValue > 126){
              //cap the pump value because the ASCII table ran out
              winston.log("warn", "Tried to command too high of an additive pump value");
              pumpValue = 126;
            }
            additiveArray[index] = String.fromCharCode(pumpValue);
            break;
          default:
            //Something happened, give it a 0
            winston.log("warn", "energy commanded something other than a 1,2,or 3", caffeineValue);
            additiveArray[index] = String.fromCharCode(48);
            break;
        }
      }
      index = index + 1;
      if(index == 6){
        winston.log("debug", "additiveArray", additiveArray);
        var additiveCode = "";
        var count = 1;
        var length = additiveArray.length;
        additiveArray.forEach(function(additiveChar){
          additiveCode = additiveCode + additiveChar;
          count = count + 1;
        });
        if(count >= length){
          winston.log("debug", "additive code", additiveCode);
          resolve(additiveCode);
        }
      }
    });
  });
};


module.exports = RefillAdditive;
