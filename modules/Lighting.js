var util = require("util");
var BaseModule = require("./BaseModule");

/**
  @class Lighting
  @constructor
  @extends BaseModule
  @param {SerialPort} serialPort
*/
function Lighting (serialPort) {
  this.serialPort = serialPort;
  this.moduleName = "Lighting";
}
util.inherits(Lighting, BaseModule);


Lighting.prototype.unlockDoor = function() {
  // Write directly to serial port outside of BaseModule's command-response system
  this.serialPort.write("U");
};

Lighting.prototype.turnOnUVLight = function() {
  // Write directly to serial port outside of BaseModule's command-response system
  this.serialPort.write("V");
  return this.checkStatus("K");
};


Lighting.prototype.turnOffUVLight = function() {
  // Write directly to serial port outside of BaseModule's command-response system
  this.serialPort.write("B");
  return this.checkStatus("K");
};


Lighting.prototype.checkRefillEnabled = function(){
  var self = this;
  return new Promise(function(resolve){
    self.writeWithoutLogs("F");
    self.createTimeoutForEvent("F");
    resolve(self.checkStatusWithoutLogs("YN"));
  });
};

module.exports = Lighting;
