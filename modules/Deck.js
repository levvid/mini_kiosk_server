var util = require("util");
var BaseModule = require("./BaseModule");

/**
  @class Deck
  @extends BaseModule
  @constructor
  @param {SerialPort} serialPort The open serial port.
*/
function Deck (serialPort) {
  this.serialPort = serialPort;
  this.moduleName = "Deck";
}
util.inherits(Deck, BaseModule);

Deck.prototype.sendOrder = function (temperature) {
  var code = "";
  if(temperature == "room"){
    code = "8";
  }else{
    code = "0";
  }
  this.write(code);
  return this.checkStatus("K");
};

Deck.prototype.uncapBottle = function() {
  this.write("U");
  this.createTimeoutForEvent("U");
  return this.checkStatus("K");
};

Deck.prototype.capCheck = function() {
  this.write("Q");
  // C: Cap is present in sensor
  // N: No cap is detected
  this.createTimeoutForEvent("Q");
  return this.checkStatus("CN");
};

Deck.prototype.capBottle = function() {
  this.write("C");
  this.createTimeoutForEvent("C");
  return this.checkStatus("K");
};

Deck.prototype.moveToDropOff = function() {
  this.write("D");
  this.createTimeoutForEvent("D");
  return this.checkStatus("K");
};

Deck.prototype.moveToGarbage = function() {
  this.write("G");
  this.createTimeoutForEvent("G");
  return this.checkStatus("K");
};

Deck.prototype.moveToElevator = function() {
  this.write("E");
  this.createTimeoutForEvent("E");
  return this.checkStatus("K");
};

Deck.prototype.moveToFiller = function() {
  this.write("F");
  this.createTimeoutForEvent("F");
  return this.checkStatus("K");
};

module.exports = Deck;
