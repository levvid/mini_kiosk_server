var util = require("util");
var BaseModule = require("./BaseModule");

/**
  @class Reception
  @constructor
  @extends BaseModule
  @param {SerialPort} serialPort
*/
function Reception (serialPort) {
  this.serialPort = serialPort;
  this.moduleName = "Reception";
}
util.inherits(Reception, BaseModule);

// Individual commands

Reception.prototype.moveToDeck = function() {
  this.write("U");
  this.createTimeoutForEvent("U");
  return this.checkStatus("K");
};

Reception.prototype.moveToDoor = function() {
  this.write("D");
  this.createTimeoutForEvent("D");
  return this.checkStatus("K");
};

Reception.prototype.moveToCapperForCapping = function() {
  this.write("L");
  return this.checkStatus("K");
};

Reception.prototype.moveToCapperForUncapping = function() {
  this.write("P");
  return this.checkStatus("K");
};

Reception.prototype.synchronizeCapOn = function() {
  this.write("S");
  return this.checkStatus("K");
};

Reception.prototype.synchronizeCapOff = function() {
  this.write("M");
  return this.checkStatus("K");
};

Reception.prototype.open = function() {
  this.write("O");
  this.createTimeoutForEvent("O");
  return this.checkStatus("K");
};

Reception.prototype.close = function() {
  this.write("C");
  return this.checkStatus("K");
};

// Combined commands

Reception.prototype.closeThenUp = function() {
  var self = this;
  return this.close().then(function() {
    return self.moveToDeck();
  });
};

module.exports = Reception;
