"use strict";

var postal = require("postal");
var winston = require("winston");
const User = require("../models/User");
const mongoose = require("mongoose");
const db = require("../config/staging_keys").mongoURI;
var http = require("http");
var io = require("socket.io")(http);

// Connect to MongoDB
mongoose
  .connect(db, { useNewUrlParser: true })
  .then(() => console.log("Staging DB successfully connected"))
  .catch((err) => console.log(err));

var postalChannel = postal.channel("scanner");

/**
  The key to unlock the door of the machine.
  @property UNLOCK_CODE
  @type String
  @static
  @final
*/
Scanner.UNLOCK_CODE = "0E6C8C3F-26F9-4D65-ADC1-00F88286F64B";

/**
  The Scanner module is responsible for parsing incoming data from the scanner's
  serial port. It will publish appropriate messages on postal to the "scanner"
  channel.
  @class Scanner
  @param {SerialPort} serialPort The open serial port for the scanner.
  @constructor
*/
/**
  This message is published on postal when the scanner has read data.
  @event data
  @param {Buffer} buffer The Buffer of data.
  @static
*/
/**
  This message is published on postal when the scanner has found the unlock
  code.
  @event unlock
  @static
*/


function Scanner (scannerDevice, machineReadyForOrder) {
  var self = this;

  //Doesn't allow a scanned order unless the machine is ready
  postal.subscribe({
    channel: "kioskControl",
    topic: "ready",
    callback: function() {
      winston.log("debug", "machine ready for order BLE hit true");
      self.machineReadyForOrder = true;
    }
  });
  //Disabled scanned orders when one is in Progress
  postal.subscribe({
    channel: "drinkBeingMade",
    topic: "ready",
    callback: function () {
      winston.log("debug", "machine ready for order Scanner hit true");
      self.machineReadyForOrder = true;
    }
  });

  //Disabled scanned orders when one is in Progress
  postal.subscribe({
    channel: "drinkBeingMade",
    topic: "inProgress",
    callback: function () {
      winston.log("debug", "machine ready for order BLE hit false");
      self.machineReadyForOrder = false;
    }
  });


  var stringCode = "";
  scannerDevice.on("data", function (data) {
    var nextDataPiece = codeLookup(data);
    if(nextDataPiece == "ENTER"){
      winston.log("info", "this is the QR Code: " + stringCode);
      // Check against unlock key
      // to be deprecated after new workflow comes into effect - TODO (Gibson)
      if (Scanner._checkForUnlock(stringCode)) {
        postalChannel.publish("unlock");
        // If it was an unlock command, return out of statement, do not process data
        stringCode = "";
        return;
      }
      // check for admin privileges
      else if (Scanner._checkForAdmin(stringCode)) {
        winston.log("debug", "Entering maintenance");
        // enterMaintenanceMode();
        winston.log("debug", "Switched to maintenance mode...");
        stringCode = "";
        return;
      }
      else {
        winston.log("debug", "machine ready for order:", machineReadyForOrder);
        if(self.machineReadyForOrder == true){
          winston.log("verbose", "QR Scanner read a code.");
          postalChannel.publish("data", stringCode);
        }
      }
      stringCode = "";
    }else{
      stringCode = stringCode + nextDataPiece;
    }
  });
}

/**
  the data table for the HID lookup from the scanner
  @method codeLookup
  @param {array} code This array has the data that corresponds to the lookup table
  @static
  @private
*/

function codeLookup(code){
  var thirdDataLookupTable = ["", "", "", "", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "ENTER", "ESCAPE", "DELETE", "BACKSPACE", "    ", "-", "=", "[", "]", "\\", "#", ";", "'", "`", ",", ".", "/"];
  var fourthDataLookupTable = ["", "", "", "", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "ENTER", "ESCAPE", "DELETE", "BACKSPACE", "    ", "_", "+", "{", "}", "|", "~",":", '"', "~", "<", ">", "?"];
  if(code[0] == 0){
    return thirdDataLookupTable[code[2]];
  }else{
    if(code[0] == 32){
      return fourthDataLookupTable[code[2]];
    }else{
      return "";
    }
  }
}

/**
  Checks the passed in data for the presence of the unlock code.
  @method _checkForUnlock
  @param {String} stringCode The data to check for the unlock code.
  @return {Bool} True if unlock key is present in data.
  @static
  @private
*/
Scanner._checkForUnlock = function (stringCode) {
  winston.log("debug", "checking for unlock with", stringCode);
  // Return true if the unlock key is present in the data string
  // Ignores line ending characters (e.g. carriage return)
  return stringCode.indexOf(Scanner.UNLOCK_CODE) !== -1;
};


Scanner._checkForAdmin = function(stringCode) {
  winston.log("debug", "Checking for QRCode admin privileges with", stringCode);
  User.findOne({ uuid: stringCode}).then((user) => {
    if (user) {
      winston.log("debug", "User " + user.name + " is accessing maintenance mode");
      postalChannel.publish("enterMaintenance");
      winston.log("debug", "User " + user.name + " is in maintenance mode");
      
      return true;
    }
    return false;
  });
}

module.exports = Scanner;
