var RSVP = require("rsvp");
var util = require("util");
var BaseModule = require("./BaseModule");
var postal = require("postal");
var jsonfile = require("jsonfile");
var winston = require("winston");
const MODULE_NAME = "Additive";
var postalChannel = postal.channel(MODULE_NAME.toLowerCase());

/**
  @class Additive
  @extends BaseModule
  @constructor
  @param {SerialPort} serialPort
  @param {Array} additives This array should contain objects, each of which
  corresponds to one additive dispenser. Each object should contain `name`, the
  name of the additive, and `remaining`, the number of doses left in that
  additive's dispenser. The order and number of elements in the array correpond
  to the order and number of additive dispensers in the machine. If an additive
  dispenser should be ignored, set its corresponding array entry to `null`. This
  array determines the mapping from a drink order, which contains additive
  names, to an additive module code, which is based on dispenser positions
  within the additive hardware module.
*/

function Additive (serialPort, additives) {
  this.serialPort = serialPort;
  this.moduleName = MODULE_NAME;

  /**
    An array containing a copy of each of the objects in the `additives`
    array passed to the constructor. The array entries are copied to avoid
    sharing state with the code invoking the constructor.
    @property additivesArray
    @type Array
  */
  var additivesArray = [];
  additives.forEach(function(additive) {
    var entry;
    if (additive === null) {
      entry = null;
    } else {
      entry = {
        name: additive.name,
        remaining: additive.remaining,
        concentration: additive.concentration
      };
    }
    additivesArray.push(entry);
  });
  this.additivesArray = additivesArray;
}
util.inherits(Additive, BaseModule);

/**
  This method will retrieve the current additive volumes for the machine and
  determine if the order can be fulfilled.
  It returns a promise that resolves with `true` if there are enough additives.
  @method ableToFulfill
  @param {Order} order Check against additives needed for this order.
  @return {Promise} Resolves with `true` if enough additives, else `false`.
*/
Additive.prototype.ableToFulfill = function (order) {
  order; // Silence linter until function is fixed
  return RSVP.resolve(true); // FIXME
};

/**
  Allow testing of logic for `ableToFulfill()` method.
  @method _ableToFulfill
  @private
  @static
*/
Additive._ableToFulfill = function (order, volumes) {
  // Check there is enough volume for every requested additive
  return order.getAdditiveCodeArray().every(function (codeValue, index) {
    return codeValue <= volumes[index];
  });
};

/**
  @method sendOrder
  @param {Order} order Fill additives for this order.
  @return {Promise} Resolves when additives module is done.
*/
Additive.prototype.sendOrder = function (flavor, caffeineValue) {
  // Check if this order has additives
  var self = this;
  return new Promise(function(resolve){
    self._generateAdditiveCode(flavor, caffeineValue).then(function(additiveCode){
      self.write(additiveCode);
      self.createTimeoutForEvent(additiveCode);
      self.updateVolumes(flavor, caffeineValue);
      resolve(self.checkStatus("K"));
    });
  });


};

/**
  Generate an additive code corresponding to the order object. The additive code
  is a string in which each character corresponds to the amount to dispense from
  the pump in the position corresponding to that character's index in the string.
  @method _generateAdditiveCode
  @param {Order} order Generate the code for this order.
  @return {String} The generated additive code.
  @private
*/
Additive.prototype._generateAdditiveCode = function (flavor, caffeineValue) {
  winston.log("debug", "flavor", flavor);
  winston.log("debug", "caffeine", caffeineValue);

  return new Promise(function(resolve, reject){
    var code = "";
    var jsonInventory = jsonfile.readFileSync("inventory.json");
    var additiveArray = ["0", "0", "0", "0", "0", "0", "0", "0"];
    var additiveLookup = jsonfile.readFileSync("additiveLookup.json");
    var index = 0;
    jsonInventory.additives.forEach(function(inventoryObject){
      if(inventoryObject.name == flavor){
        var mlPerNum = getmLPerDose(flavor)
        additiveLookup.additives.forEach(function(additiveLookupObject){
          if(flavor == additiveLookupObject.name){
            var concentration = additiveLookupObject.concentration;
            var desiredPumpValue = Math.round(concentration/mlPerNum);
            desiredPumpValue = desiredPumpValue + 48;
            if(desiredPumpValue > 126){
              //Did this because the ASCII table ran out
              winston.log("warn", "Tried to command too high of an additive pump value");
              desiredPumpValue = 126;
            }
            if(String.fromCharCode(desiredPumpValue) == "?" || String.fromCharCode(desiredPumpValue) == "X"){
              desiredPumpValue = desiredPumpValue + 1;
            }
            char = String.fromCharCode(desiredPumpValue);
            additiveArray[index] = char;
          }
        });
      }
      if(inventoryObject.name == "caffeine"){
        var mlPerNum = getmLPerDose("caffeine");
        switch(caffeineValue){
          case 0:
            additiveArray[index] = String.fromCharCode(48);
            break;
          case 1:
            //20mg
            var pumpValue = Math.round(1.68 / mlPerNum);
            pumpValue = pumpValue + 48;
            if(pumpValue > 126){
              //cap the pump value because the ASCII table ran out
              winston.log("warn", "Tried to command too high of an additive pump value");
              pumpValue = 126;
            }
            additiveArray[index] = String.fromCharCode(pumpValue);
            break;
          case 2:
            //40mg
            var pumpValue = Math.round(3.37 / mlPerNum);
            pumpValue = pumpValue + 48;
            if(pumpValue > 126){
              //cap the pump value because the ASCII table ran out
              winston.log("warn", "Tried to command too high of an additive pump value");
              pumpValue = 126;
            }
            additiveArray[index] = String.fromCharCode(pumpValue);
            break;
          case 3:
            //70mg
            var pumpValue = Math.round(5.9 / mlPerNum);
            pumpValue = pumpValue + 48;
            if(pumpValue > 126){
              //cap the pump value because the ASCII table ran out
              winston.log("warn", "Tried to command too high of an additive pump value");
              pumpValue = 126;
            }
            additiveArray[index] = String.fromCharCode(pumpValue);
            break;
          default:
            //Something happened, give it a 0
            winston.log("warn", "energy commanded something other than a 1,2,or 3", caffeineValue);
            additiveArray[index] = String.fromCharCode(48);
            break;
        }
      }
      index = index + 1;
      if(index == 6){
        winston.log("debug", "additiveArray", additiveArray);
        var additiveCode = "";
        var count = 1;
        var length = additiveArray.length;
        additiveArray.forEach(function(additiveChar){
          additiveCode = additiveCode + additiveChar;
          count = count + 1;
        });
        if(count >= length){
          winston.log("debug", "additive code", additiveCode);
          resolve(additiveCode);
        }
      }
    });
  });
};

/**
  Update the remaining additive quantities in the additives array based on the
  names and quantities of additives used to fulfill the specified order.
  This method publishes `additivesDispensed` notifications to the `additive`
  postal channel. The message's data is an array of objects,
  of just the additives that have been updated, and in each object are:
  * `name`: the name of the dispensed additive
  * `remaining`: the quantity of the additive remaining, in doses
  * `index`: the 0-based position of the additive in the array
  @method updateVolumes
  @param {Order} order Update the additive volumes based on fulfilling this
  order.
*/
Additive.prototype.updateVolumes = function (flavor, caffeineValue) {
  var additiveUpdates = [];
  var self = this;
  var jsonInventory = jsonfile.readFileSync("inventory.json");
  var concentrationJson = jsonfile.readFileSync("additiveLookup.json");
  var index = 0
  jsonInventory.additives.forEach(function(additive){
    if(additive.name == flavor){
      concentrationJson.additives.forEach(function(concentrationObject){
        if(concentrationObject.name == flavor){
          additive.remaining = additive.remaining - concentrationObject.concentration;
          jsonInventory.additives[index] = additive;
        }
      })
    }
    if(additive.name == "caffeine"){
      var quantity = 0;
      if(caffeineValue == 1){
        //20mg of caffeine requested is 1.68 ml of solution
        quantity = 1.68;
      }else if(caffeineValue == 2){
        //40mg of caffeine requested is 3.37 ml of solution
        quantity = 3.37;
      }else if(caffeineValue == 3){
        //70mg of caffeine requested is 5.9 ml of solution
        quantity = 5.9;
      }
      additive.remaining = additive.remaining - quantity;
      jsonInventory.additives[index] = additive;
    }
    index = index + 1;
  })
  jsonfile.writeFileSync("inventory.json", jsonInventory, {spaces: 2});

};

function getmLPerDose(additiveName){
  var dispenseRateJson = jsonfile.readFileSync("additive-dispense-rate.json");
  winston.log("debug", "additiveName",additiveName);
  var count = 0;
  while(count < dispenseRateJson.length){
    if(dispenseRateJson[count].name == additiveName){
      return dispenseRateJson[count].mlPerNum;
    }
    count++;
  }
}

/**
  This method is only for use on the Fountain machine. It tells the Additive
  module to stop dispensing.
  @method endDispense
*/
Additive.prototype.endDispense = function() {
  this.write("O");
  return this.checkStatus("K");
};

/**
  This method is only for use on the Fountain machine. It tells the Additive
  module to stop dispensing.
  @method purgeAdditives
  @param purgeCommand
*/
Additive.prototype.purgeAdditives = function(additives){
  var orderString = "";
  winston.log("info", "additives", additives);
  additives.forEach(function(additive){
    winston.log("info", "additive.purge", additive.purge);
    var char;
    if(additive.purge == 1){
      //ToDo: placeholder sending 1 until I work out with ron the actual value to send
      char = "1";
    }else{
    //additive.purge == 0
      char = "0";
    }
    orderString += char;
  });
  winston.log("info", "Purging additives", orderString);
  orderString = orderString + "0000";
  this.write(orderString);

  return this.checkStatus("K");
};

module.exports = Additive;
