// Runs as a separate Node process
// Subscribe to the PM2 bus logging messages

const pm2 = require("pm2");
var winston = require("winston");
const loggly = require("node-loggly-bulk");
const os = require("os");
var postal = require("postal");
var postalChannel = postal.channel("log-service");

// Setup Loggly
const logglyClient = loggly.createClient({
  token: "ddeac586-338e-40d3-9831-b9c651460208",
  subdomain: "dropwater",
  tags: ["node-log-service", os.hostname()],
  json: true
});

pm2.launchBus((error, pm2bus) => {
  if (error) {
    postalChannel.publish("logServiceError", "error");
    throw error;
  }
  pm2bus.on("winston:log", function (event) {
    const data = event.data;

    const logObject = data.meta || {};
    logObject.level = data.level;
    logObject.message = data.msg;

    

    logglyClient.log(logObject);
  });
});
